#!/usr/bin/python3

import unittest
import z3

from Agent import AgentConstraint, MultiAgentSystem
from Formula import Future, Forall, Exists, Atom, Quant, Bind, Always
from JsonOutputFormatter import FormatModelInJson, FormatUnsatMessage


class TestJsonOutputFormatter(unittest.TestCase):
    def InitMas(self):
        AgentsConstraints = []
        AgentsConstraints.append(AgentConstraint(3, 2, 1))
        AgentsConstraints.append(AgentConstraint(2, 2, 1))
        Mas = MultiAgentSystem()
        for c in AgentsConstraints:
            Mas.AddAgent(c)
        Mas.SetInitialState(0, 0)
        Mas.ComputeModel()
        return Mas

    def InitModel(self, mas):
        model = z3.Model()
        for a in mas.agents:
            pb = [i for sl in a.pb for i in sl]
            vb = [i for sl in a.vb for i in sl]
            tb = [i for ssl in a.tb for sl in ssl for i in sl]
            for b in pb + tb + vb:
                model.update_value(b, False)

        m = model
        a1 = mas.agents[0]
        m.update_value(a1.pb[0][0], True)
        m.update_value(a1.pb[1][0], True)
        m.update_value(a1.pb[2][0], True)
        m.update_value(a1.pb[2][1], True)
        m.update_value(a1.tb[0][0][1], True)
        m.update_value(a1.tb[0][1][2], True)
        m.update_value(a1.tb[1][0][2], True)
        m.update_value(a1.tb[1][1][2], True)
        m.update_value(a1.tb[2][0][1], True)
        m.update_value(a1.tb[2][1][1], True)
        m.update_value(a1.tb[2][3][1], True)
        m.update_value(a1.tb[2][2][0], True)
        m.update_value(a1.vb[1][0], True)

        a2 = mas.agents[1]
        m.update_value(a2.pb[0][0], True)
        m.update_value(a2.pb[0][1], True)
        m.update_value(a2.pb[1][1], True)
        m.update_value(a2.tb[0][0][0], True)
        m.update_value(a2.tb[0][2][0], True)
        m.update_value(a2.tb[0][1][1], True)
        m.update_value(a2.tb[0][3][1], True)
        m.update_value(a2.tb[0][0][0], True)
        m.update_value(a2.tb[1][1][1], True)
        m.update_value(a2.tb[1][3][1], True)
        m.update_value(a2.vb[1][0], True)

        return model

    def test_FormatUnsatMessage_AnyMas_ReturnsProperMessage(self):
        mas = self.InitMas()
        output = FormatUnsatMessage(mas)
        expectedOutput = '{"info": {"status": "UNSAT"}, "models": []}'
        self.assertEqual(output, expectedOutput)

    def test_FormatModelInJson_Mas_ReturnsProperMessage(self):
        mas = self.InitMas()
        model = self.InitModel(mas)
        output = FormatModelInJson(mas, model)
        expectedOutput = '{"info": {"status": "SAT"}, "models": [{"label": "Agent 0", "nodes": [{"id": 0, "label": "0", "props": []}, {"id": 1, "label": "1", "props": [0]}, {"id": 2, "label": "2", "props": []}], "links": [{"id": 0, "source": 0, "target": 1, "label": "[0]"}, {"id": 1, "source": 0, "target": 2, "label": "[1]"}, {"id": 2, "source": 1, "target": 2, "label": "[0, 1]"}, {"id": 3, "source": 2, "target": 0, "label": "[2]"}, {"id": 4, "source": 2, "target": 1, "label": "[0, 1, 3]"}]}, {"label": "Agent 1", "nodes": [{"id": 0, "label": "0", "props": []}, {"id": 1, "label": "1", "props": [0]}], "links": [{"id": 0, "source": 0, "target": 0, "label": "[0, 2]"}, {"id": 1, "source": 0, "target": 1, "label": "[1, 3]"}, {"id": 2, "source": 1, "target": 1, "label": "[1, 3]"}]}, {"label": "Global model", "nodes": [{"id": 0, "label": "[0, 0]", "props": []}, {"id": 1, "label": "[0, 1]", "props": ["1.0"]}, {"id": 2, "label": "[1, 0]", "props": ["0.0"]}, {"id": 3, "label": "[1, 1]", "props": ["0.0", "1.0"]}, {"id": 4, "label": "[2, 0]", "props": []}, {"id": 5, "label": "[2, 1]", "props": ["1.0"]}], "links": [{"id": 0, "source": 0, "target": 2, "label": "[0]"}, {"id": 1, "source": 0, "target": 5, "label": "[1]"}, {"id": 2, "source": 1, "target": 5, "label": "[1]"}, {"id": 3, "source": 2, "target": 4, "label": "[0]"}, {"id": 4, "source": 2, "target": 5, "label": "[1]"}, {"id": 5, "source": 3, "target": 5, "label": "[1]"}, {"id": 6, "source": 4, "target": 0, "label": "[2]"}, {"id": 7, "source": 4, "target": 2, "label": "[0]"}, {"id": 8, "source": 4, "target": 3, "label": "[1, 3]"}, {"id": 9, "source": 5, "target": 3, "label": "[1, 3]"}]}]}'
        self.assertEqual(output, expectedOutput)


if __name__ == "__main__":
    unittest.main()
