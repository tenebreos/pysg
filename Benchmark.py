#!/usr/bin/python3

# Benchmark.py: universal benchmarking facilities.

from Config import GetConfig

import time


class Benchmark:
    def __init__(self):
        self.runtime = 0
        self.result_file = GetConfig().BenchmarkFilename()
        self.total = 0

    def Start(self):
        self.runtime = time.time_ns()

    def Stop(self):
        self.total += time.time_ns() - self.runtime

    def Save(self, label):
        results = open(self.result_file, 'a')
        results.write(f'{label} = {self.total}\n')
        results.close()
