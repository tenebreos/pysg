#!/usr/bin/python3

# Formula: a tree representation of a SL[SG] formula and (soon^tm) a parser for
# it.

# TODO: turn this into a dict, and make other parts use it.
UnaryBinaryOps = ["Not"]
BinaryBinaryOps = ["And", "Or", "Implies", "Iff"]
UnaryTemporalOps = ["Next", "Future", "Always"]
BinaryTemporalOps = ["Until"]


class Formula:
    def __init__(self):
        self.quant = None
        self.bind = None
        self.op = None
        self.arg1 = None
        self.arg2 = None

    def __str__(self):
        return (f'Formula(quant={self.quant}, ' +
                f'bind={self.bind}, ' +
                f'op={self.op}, ' +
                f'arg1={self.arg1}, ' +
                f'arg2={self.arg2})')

    def __eq__(self, other):
        return (other is not None and
                self.quant == other.quant and
                self.bind == other.bind and
                self.op == other.op and
                self.arg1 == other.arg1 and
                self.arg2 == other.arg2)

    def __agentsUnderQuantification(self, quant_type):
        TemporalOps = UnaryTemporalOps + BinaryTemporalOps
        if self.op not in TemporalOps:
            raise ValueError("Error retrieving agents. Only temporal "
                             "operators have quantification and binding "
                             "prefixes.")
        svars = []
        for q in self.quant:
            if q.quantifier_type == quant_type:
                svars.append(q.var)
        agents = []
        for var, a in self.bind:
            if var in svars:
                agents.append(a)
        return agents

    def AgentsUnderExistentialQuantification(self):
        return self.__agentsUnderQuantification('Exists')

    def AgentsUnderUniversalQuantification(self):
        return self.__agentsUnderQuantification('Forall')


def IsValid(formula):
    if formula is None:
        return True
    valid = True
    if isinstance(formula, Atom):
        return True
    if formula.op in UnaryBinaryOps:
        valid = (formula.quant is None and
                 formula.bind is None and
                 formula.arg1 is not None and
                 formula.arg2 is None)
    elif formula.op in BinaryBinaryOps:
        valid = (formula.quant is None and
                 formula.bind is None and
                 formula.arg1 is not None and
                 formula.arg2 is not None)
    elif formula.op in UnaryTemporalOps:
        valid = (type(formula.quant) is Quant and
                 type(formula.bind) is Bind and
                 formula.arg1 is not None and
                 formula.arg2 is None)
    elif formula.op in BinaryTemporalOps:
        valid = (type(formula.quant) is Quant and
                 type(formula.bind) is Bind and
                 formula.arg1 is not None and
                 formula.arg2 is None)
    else:
        valid = False

    return valid and IsValid(formula.arg1) and IsValid(formula.arg2)


def Not(arg):
    """
    arg --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Not"
    f.arg1 = arg
    return f


def And(*args):
    """
    arg1 --- a Formula or a string representing an atom
    arg2 --- a Formula or a string representing an atom
    """
    args = args[::-1]
    last = args[0]
    for a in args[1:]:
        f = Formula()
        f.op = "And"
        f.arg1 = a
        f.arg2 = last
        last = f
    return f


def Or(arg1, arg2):
    """
    arg1 --- a Formula or a string representing an atom
    arg2 --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Or"
    f.arg1 = arg1
    f.arg2 = arg2
    return f


def Implies(arg1, arg2):
    """
    arg1 --- a Formula or a string representing an atom
    arg2 --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Implies"
    f.arg1 = arg1
    f.arg2 = arg2
    return f


def Iff(arg1, arg2):
    """
    arg1 --- a Formula or a string representing an atom
    arg2 --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Iff"
    f.arg1 = arg1
    f.arg2 = arg2
    return f


def Next(quant, bind, arg):
    """
    arg --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Next"
    f.arg1 = arg
    f.quant = quant
    f.bind = bind
    return f


def Until(quant, bind, arg1, arg2):
    """
    arg1 --- a Formula or a string representing an atom
    arg2 --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Until"
    f.arg1 = arg1
    f.arg2 = arg2
    f.quant = quant
    f.bind = bind
    return f


def Future(quant, bind, arg):
    """
    arg --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Future"
    f.arg1 = arg
    f.quant = quant
    f.bind = bind
    return f


# TODO: rename this to "Global", for consistent naming.
def Always(quant, bind, arg):
    """
    arg --- a Formula or a string representing an atom
    """
    f = Formula()
    f.op = "Always"
    f.arg1 = arg
    f.quant = quant
    f.bind = bind
    return f


class Quantifier:
    def __init__(self, quantifier_type, strategy_var):
        if quantifier_type not in ['Forall', 'Exists']:
            raise ValueError(f'Invalid quantifier type: {quantifier_type}. '
                             f'Valid types: Forall, Exists.')
        self.var = strategy_var
        self.quantifier_type = quantifier_type

    def __str__(self):
        short_type = ('A' if self.quantifier_type == 'Forall' else 'E')
        return f'<{short_type} {self.var}>'

    def __eq__(self, other):
        return (self.var == other.var and
                self.quantifier_type == other.quantifier_type)


def Bind(*bindings):
    out = []
    for b in bindings:
        var, agent = b
        if type(var) is not str:
            raise ValueError()
        if type(agent) is not int:
            raise ValueError()
        out.append(b)
    return out


def Quant(*quantifications):
    out = []
    for q in quantifications:
        if type(q) is not Quantifier:
            raise ValueError()
        out.append(q)
    return out


def Forall(strategy_var):
    return Quantifier('Forall', strategy_var)


def Exists(strategy_var):
    return Quantifier('Exists', strategy_var)


class Atom:
    def __init__(self, agent_id, prop_id):
        self.agent_id = agent_id
        self.prop_id = prop_id

    def __str__(self):
        return f'{self.agent_id}.{self.prop_id}'

    def __eq__(self, other):
        return (self.agent_id == other.agent_id and
                self.prop_id == other.prop_id)


if __name__ == '__main__':
    Next(Quant(Forall('x'), Exists('y')), Bind(('x', 1), ('y', 2)), Atom(0, 0))
