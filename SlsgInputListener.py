# Generated from SlsgInput.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SlsgInputParser import SlsgInputParser
else:
    from SlsgInputParser import SlsgInputParser

# This class defines a complete listener for a parse tree produced by SlsgInputParser.
class SlsgInputListener(ParseTreeListener):

    # Enter a parse tree produced by SlsgInputParser#input_file.
    def enterInput_file(self, ctx:SlsgInputParser.Input_fileContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#input_file.
    def exitInput_file(self, ctx:SlsgInputParser.Input_fileContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#line.
    def enterLine(self, ctx:SlsgInputParser.LineContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#line.
    def exitLine(self, ctx:SlsgInputParser.LineContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#agent_spec.
    def enterAgent_spec(self, ctx:SlsgInputParser.Agent_specContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#agent_spec.
    def exitAgent_spec(self, ctx:SlsgInputParser.Agent_specContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#agent_id.
    def enterAgent_id(self, ctx:SlsgInputParser.Agent_idContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#agent_id.
    def exitAgent_id(self, ctx:SlsgInputParser.Agent_idContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#local_states_no.
    def enterLocal_states_no(self, ctx:SlsgInputParser.Local_states_noContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#local_states_no.
    def exitLocal_states_no(self, ctx:SlsgInputParser.Local_states_noContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#local_actions_no.
    def enterLocal_actions_no(self, ctx:SlsgInputParser.Local_actions_noContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#local_actions_no.
    def exitLocal_actions_no(self, ctx:SlsgInputParser.Local_actions_noContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#local_props_no.
    def enterLocal_props_no(self, ctx:SlsgInputParser.Local_props_noContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#local_props_no.
    def exitLocal_props_no(self, ctx:SlsgInputParser.Local_props_noContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#formula.
    def enterFormula(self, ctx:SlsgInputParser.FormulaContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#formula.
    def exitFormula(self, ctx:SlsgInputParser.FormulaContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaFuture.
    def enterFormulaFuture(self, ctx:SlsgInputParser.FormulaFutureContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaFuture.
    def exitFormulaFuture(self, ctx:SlsgInputParser.FormulaFutureContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaGlobal.
    def enterFormulaGlobal(self, ctx:SlsgInputParser.FormulaGlobalContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaGlobal.
    def exitFormulaGlobal(self, ctx:SlsgInputParser.FormulaGlobalContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaNext.
    def enterFormulaNext(self, ctx:SlsgInputParser.FormulaNextContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaNext.
    def exitFormulaNext(self, ctx:SlsgInputParser.FormulaNextContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaUntil.
    def enterFormulaUntil(self, ctx:SlsgInputParser.FormulaUntilContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaUntil.
    def exitFormulaUntil(self, ctx:SlsgInputParser.FormulaUntilContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaRelease.
    def enterFormulaRelease(self, ctx:SlsgInputParser.FormulaReleaseContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaRelease.
    def exitFormulaRelease(self, ctx:SlsgInputParser.FormulaReleaseContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaAnd.
    def enterFormulaAnd(self, ctx:SlsgInputParser.FormulaAndContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaAnd.
    def exitFormulaAnd(self, ctx:SlsgInputParser.FormulaAndContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaOr.
    def enterFormulaOr(self, ctx:SlsgInputParser.FormulaOrContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaOr.
    def exitFormulaOr(self, ctx:SlsgInputParser.FormulaOrContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaImpl.
    def enterFormulaImpl(self, ctx:SlsgInputParser.FormulaImplContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaImpl.
    def exitFormulaImpl(self, ctx:SlsgInputParser.FormulaImplContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaNot.
    def enterFormulaNot(self, ctx:SlsgInputParser.FormulaNotContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaNot.
    def exitFormulaNot(self, ctx:SlsgInputParser.FormulaNotContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaProp.
    def enterFormulaProp(self, ctx:SlsgInputParser.FormulaPropContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaProp.
    def exitFormulaProp(self, ctx:SlsgInputParser.FormulaPropContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaTrue.
    def enterFormulaTrue(self, ctx:SlsgInputParser.FormulaTrueContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaTrue.
    def exitFormulaTrue(self, ctx:SlsgInputParser.FormulaTrueContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#FormulaFalse.
    def enterFormulaFalse(self, ctx:SlsgInputParser.FormulaFalseContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#FormulaFalse.
    def exitFormulaFalse(self, ctx:SlsgInputParser.FormulaFalseContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#prop.
    def enterProp(self, ctx:SlsgInputParser.PropContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#prop.
    def exitProp(self, ctx:SlsgInputParser.PropContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#prop_id.
    def enterProp_id(self, ctx:SlsgInputParser.Prop_idContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#prop_id.
    def exitProp_id(self, ctx:SlsgInputParser.Prop_idContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#quant_pref.
    def enterQuant_pref(self, ctx:SlsgInputParser.Quant_prefContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#quant_pref.
    def exitQuant_pref(self, ctx:SlsgInputParser.Quant_prefContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#quant.
    def enterQuant(self, ctx:SlsgInputParser.QuantContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#quant.
    def exitQuant(self, ctx:SlsgInputParser.QuantContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#quantifier.
    def enterQuantifier(self, ctx:SlsgInputParser.QuantifierContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#quantifier.
    def exitQuantifier(self, ctx:SlsgInputParser.QuantifierContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#bind_pref.
    def enterBind_pref(self, ctx:SlsgInputParser.Bind_prefContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#bind_pref.
    def exitBind_pref(self, ctx:SlsgInputParser.Bind_prefContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#bind.
    def enterBind(self, ctx:SlsgInputParser.BindContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#bind.
    def exitBind(self, ctx:SlsgInputParser.BindContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#override.
    def enterOverride(self, ctx:SlsgInputParser.OverrideContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#override.
    def exitOverride(self, ctx:SlsgInputParser.OverrideContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#val_override.
    def enterVal_override(self, ctx:SlsgInputParser.Val_overrideContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#val_override.
    def exitVal_override(self, ctx:SlsgInputParser.Val_overrideContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#prot_override.
    def enterProt_override(self, ctx:SlsgInputParser.Prot_overrideContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#prot_override.
    def exitProt_override(self, ctx:SlsgInputParser.Prot_overrideContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#trans_override.
    def enterTrans_override(self, ctx:SlsgInputParser.Trans_overrideContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#trans_override.
    def exitTrans_override(self, ctx:SlsgInputParser.Trans_overrideContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#local_state_id.
    def enterLocal_state_id(self, ctx:SlsgInputParser.Local_state_idContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#local_state_id.
    def exitLocal_state_id(self, ctx:SlsgInputParser.Local_state_idContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#local_action_id.
    def enterLocal_action_id(self, ctx:SlsgInputParser.Local_action_idContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#local_action_id.
    def exitLocal_action_id(self, ctx:SlsgInputParser.Local_action_idContext):
        pass


    # Enter a parse tree produced by SlsgInputParser#joint_action_id.
    def enterJoint_action_id(self, ctx:SlsgInputParser.Joint_action_idContext):
        pass

    # Exit a parse tree produced by SlsgInputParser#joint_action_id.
    def exitJoint_action_id(self, ctx:SlsgInputParser.Joint_action_idContext):
        pass



del SlsgInputParser