#!/usr/bin/python3

import unittest
from z3 import Solver

from Agent import AgentConstraint, MultiAgentSystem
from Phi import Phi
from Apx import ModelApproximation, SatisfingStates, Apx
from Formula import Atom, Not, And


class TestTheorySolver(unittest.TestCase):
    def FakeMas(self):
        AgentsConstraints = []
        AgentsConstraints.append(AgentConstraint(3, 3, 1))
        AgentsConstraints.append(AgentConstraint(2, 2, 2))
        Mas = MultiAgentSystem()
        for c in AgentsConstraints:
            Mas.AddAgent(c)
        Mas.SetInitialState(0, 0)
        Mas.ComputeModel()
        return Mas

    def FakeModel(self, mas):
        solver = Solver()
        solver.add(Phi(mas))
        solver.check()
        return solver.model()

    def test_ModelApproximation_underapprxCorrectForGammaEqA(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        new_model = ModelApproximation(mas, model, 'under',
                                       list(range(len(mas.agents))))
        for d in new_model:
            old_val = model[d]
            new_val = new_model[d]
            if old_val is not None:
                self.assertEqual(old_val, new_val, 'Every atom from the old '
                                 'model should have the same value in the new '
                                 'model')
            else:
                self.assertEqual(new_val, False, 'Every atom undefined in '
                                 'the old model should have True assigned.')

    def test_ModelApproximation_overappxCorrectForGammaEqA(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        new_model = ModelApproximation(mas, model, 'over',
                                       list(range(len(mas.agents))))
        for d in new_model:
            old_val = model[d]
            new_val = new_model[d]
            if old_val is not None:
                self.assertEqual(old_val, new_val, 'Every atom from the old '
                                 'model should have the same value in the new '
                                 'model')
            else:
                self.assertEqual(new_val, True, 'Every atom undefined in '
                                 'the old model should have True assigned.')

    # TODO: implement test
    def test_ModelApproximation_underappxCorrectForGammaNeqA(self):
        pass

    # TODO: implement test
    def test_ModelApproximation_overappxCorrectForGammaNeqA(self):
        pass

    def test_ModelApproximation_errorsOnInvalidApproximationType(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        inputs = ['asdf', 20, 1.1, True, 2+1j]
        for i in inputs:
            with self.assertRaises(ValueError):
                ModelApproximation(mas, model, i, [])

    def test_SatisfingStates_returnsSatisfingStates(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        valuation = [
            [
                [True, False, True],
            ],
            [
                [False, True],
                [True, False]
            ]
        ]
        for i, agent in enumerate(mas.agents):
            for a in range(agent.constraints.variablesCount):
                for j, atoms_in_state in enumerate(agent.vb):
                    model.update_value(atoms_in_state[a], valuation[i][a][j])

        atoms = [Atom(0, 0), Atom(1, 0), Atom(1, 1)]
        expectedOutputs = [
            [0, 1, 4, 5],
            [1, 3, 5],
            [0, 2, 4]
        ]
        for i, atom in enumerate(atoms):
            output = SatisfingStates(atom, mas, model)
            self.assertEqual(output, expectedOutputs[i])

    def test_SatisfingStates_errorsOnInvalidAgentNumber(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        with self.assertRaises(ValueError):
            SatisfingStates(Atom(21, 0), mas, model)

    def test_SatisfingStates_errorsOnInvalidAtomNumber(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        with self.assertRaises(ValueError):
            SatisfingStates(Atom(0, 21), mas, model)

    def test_Apx_worksForAtoms(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        vb = list(map(lambda a: a.vb, mas.agents))

        phi = Atom(0, 0)

        # Overapprox.:
        expectedResult = [0, 1, 2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = []
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

        model.update_value(vb[0][0][0], False)

        # Overapprox.:
        expectedResult = [2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = []
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

        model.update_value(vb[0][1][0], True)

        # Overapprox.:
        expectedResult = [2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = [2, 3]
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

    def test_Apx_worksForNot(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        phi = Not(Atom(0, 0))
        vb = list(map(lambda a: a.vb, mas.agents))

        # Overapprox.:
        expectedResult = [0, 1, 2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = []
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

        model.update_value(vb[0][0][0], False)

        # Overapprox.:
        expectedResult = [0, 1, 2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = [0, 1]
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

        model.update_value(vb[0][2][0], True)

        # Overapprox.:
        expectedResult = [0, 1, 2, 3]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = [0, 1]
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

    def test_Apx_worksForAnd(self):
        mas = self.FakeMas()
        model = self.FakeModel(mas)
        vb = list(map(lambda a: a.vb, mas.agents))

        phi = And(Atom(0, 0), Atom(1, 0))

        # Overapprox.:
        expectedResult = [0, 1, 2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = []
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

        model.update_value(vb[0][1][0], True)

        # Overapprox.:
        expectedResult = [0, 1, 2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = []
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

        model.update_value(vb[1][1][0], True)

        # Overapprox.:
        expectedResult = [0, 1, 2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = [3]
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

        model.update_value(vb[0][0][0], False)

        # Overapprox.:
        expectedResult = [2, 3, 4, 5]
        result = Apx(phi, mas, model, 'over')
        self.assertEqual(result, expectedResult)

        # Underapprox.:
        expectedResult = [3]
        result = Apx(phi, mas, model, 'under')
        self.assertEqual(result, expectedResult)

    def test_Apx_worksForNext(self):
        pass

    def test_Apx_worksForUntil(self):
        pass


if __name__ == "__main__":
    unittest.main()
    # test = TestTheorySolver()
    # mas = test.FakeMas()
    # model = test.FakeModel(mas)

    # print(model)

    # gamma = [0]
    # result = ModelApproximation(mas, model, 'under', gamma)

    # print(result)
