# Generated from SlsgInput.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2#")
        buf.write("\u00f2\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\3\2\3\2\3\2\3\2\3")
        buf.write("\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\6")
        buf.write("\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f")
        buf.write("\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22")
        buf.write("\3\22\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\27\6\27\u0095\n\27\r\27\16\27\u0096")
        buf.write("\3\30\3\30\3\30\3\30\5\30\u009d\n\30\3\31\3\31\3\31\5")
        buf.write("\31\u00a2\n\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32")
        buf.write("\u00ab\n\32\3\33\3\33\3\33\3\33\3\33\5\33\u00b2\n\33\3")
        buf.write("\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34")
        buf.write("\3\34\5\34\u00c0\n\34\3\35\3\35\3\35\3\35\3\35\3\35\3")
        buf.write("\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u00d1")
        buf.write("\n\35\3\36\3\36\7\36\u00d5\n\36\f\36\16\36\u00d8\13\36")
        buf.write("\3\37\3\37\7\37\u00dc\n\37\f\37\16\37\u00df\13\37\3\37")
        buf.write("\3\37\3\37\3\37\3 \3 \3!\6!\u00e8\n!\r!\16!\u00e9\3!\3")
        buf.write("!\3\"\5\"\u00ef\n\"\3\"\3\"\3\u00dd\2#\3\3\5\4\7\5\t\6")
        buf.write("\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20")
        buf.write("\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65")
        buf.write("\34\67\359\36;\37= ?!A\"C#\3\2\t\3\2\62;\4\2((``\4\2x")
        buf.write("x~~\3\2c|\5\2\62;aac|\4\2--//\4\2\13\13\"\"\2\u00ff\2")
        buf.write("\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3")
        buf.write("\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2")
        buf.write("\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2")
        buf.write("\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%")
        buf.write("\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2")
        buf.write("\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67")
        buf.write("\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2")
        buf.write("A\3\2\2\2\2C\3\2\2\2\3E\3\2\2\2\5L\3\2\2\2\7R\3\2\2\2")
        buf.write("\tT\3\2\2\2\13V\3\2\2\2\rX\3\2\2\2\17Z\3\2\2\2\21\\\3")
        buf.write("\2\2\2\23^\3\2\2\2\25`\3\2\2\2\27b\3\2\2\2\31d\3\2\2\2")
        buf.write("\33f\3\2\2\2\35h\3\2\2\2\37j\3\2\2\2!l\3\2\2\2#n\3\2\2")
        buf.write("\2%p\3\2\2\2\'u\3\2\2\2){\3\2\2\2+\u0082\3\2\2\2-\u0094")
        buf.write("\3\2\2\2/\u009c\3\2\2\2\61\u00a1\3\2\2\2\63\u00aa\3\2")
        buf.write("\2\2\65\u00b1\3\2\2\2\67\u00bf\3\2\2\29\u00d0\3\2\2\2")
        buf.write(";\u00d2\3\2\2\2=\u00d9\3\2\2\2?\u00e4\3\2\2\2A\u00e7\3")
        buf.write("\2\2\2C\u00ee\3\2\2\2EF\7m\2\2FG\7c\2\2GH\7i\2\2HI\7g")
        buf.write("\2\2IJ\7p\2\2JK\7v\2\2K\4\3\2\2\2LM\7m\2\2MN\7u\2\2NO")
        buf.write("\7n\2\2OP\7u\2\2PQ\7i\2\2Q\6\3\2\2\2RS\7H\2\2S\b\3\2\2")
        buf.write("\2TU\7I\2\2U\n\3\2\2\2VW\7Z\2\2W\f\3\2\2\2XY\7*\2\2Y\16")
        buf.write("\3\2\2\2Z[\7W\2\2[\20\3\2\2\2\\]\7+\2\2]\22\3\2\2\2^_")
        buf.write("\7T\2\2_\24\3\2\2\2`a\7\60\2\2a\26\3\2\2\2bc\7]\2\2c\30")
        buf.write("\3\2\2\2de\7_\2\2e\32\3\2\2\2fg\7G\2\2g\34\3\2\2\2hi\7")
        buf.write("C\2\2i\36\3\2\2\2jk\7>\2\2k \3\2\2\2lm\7.\2\2m\"\3\2\2")
        buf.write("\2no\7@\2\2o$\3\2\2\2pq\7m\2\2qr\7x\2\2rs\7c\2\2st\7n")
        buf.write("\2\2t&\3\2\2\2uv\7m\2\2vw\7r\2\2wx\7t\2\2xy\7q\2\2yz\7")
        buf.write("v\2\2z(\3\2\2\2{|\7m\2\2|}\7v\2\2}~\7t\2\2~\177\7c\2\2")
        buf.write("\177\u0080\7p\2\2\u0080\u0081\7u\2\2\u0081*\3\2\2\2\u0082")
        buf.write("\u0083\7r\2\2\u0083\u0084\7\"\2\2\u0084\u0085\7e\2\2\u0085")
        buf.write("\u0086\7p\2\2\u0086\u0087\7h\2\2\u0087\u0088\7\"\2\2\u0088")
        buf.write("\u0089\7\63\2\2\u0089\u008a\7\"\2\2\u008a\u008b\7\63\2")
        buf.write("\2\u008b\u008c\3\2\2\2\u008c\u008d\5C\"\2\u008d\u008e")
        buf.write("\7\63\2\2\u008e\u008f\7\"\2\2\u008f\u0090\7\62\2\2\u0090")
        buf.write("\u0091\3\2\2\2\u0091\u0092\5C\"\2\u0092,\3\2\2\2\u0093")
        buf.write("\u0095\t\2\2\2\u0094\u0093\3\2\2\2\u0095\u0096\3\2\2\2")
        buf.write("\u0096\u0094\3\2\2\2\u0096\u0097\3\2\2\2\u0097.\3\2\2")
        buf.write("\2\u0098\u0099\7C\2\2\u0099\u009a\7P\2\2\u009a\u009d\7")
        buf.write("F\2\2\u009b\u009d\t\3\2\2\u009c\u0098\3\2\2\2\u009c\u009b")
        buf.write("\3\2\2\2\u009d\60\3\2\2\2\u009e\u009f\7Q\2\2\u009f\u00a2")
        buf.write("\7T\2\2\u00a0\u00a2\t\4\2\2\u00a1\u009e\3\2\2\2\u00a1")
        buf.write("\u00a0\3\2\2\2\u00a2\62\3\2\2\2\u00a3\u00a4\7P\2\2\u00a4")
        buf.write("\u00a5\7G\2\2\u00a5\u00ab\7I\2\2\u00a6\u00a7\7P\2\2\u00a7")
        buf.write("\u00a8\7Q\2\2\u00a8\u00ab\7V\2\2\u00a9\u00ab\7\u0080\2")
        buf.write("\2\u00aa\u00a3\3\2\2\2\u00aa\u00a6\3\2\2\2\u00aa\u00a9")
        buf.write("\3\2\2\2\u00ab\64\3\2\2\2\u00ac\u00ad\7K\2\2\u00ad\u00ae")
        buf.write("\7O\2\2\u00ae\u00b2\7R\2\2\u00af\u00b0\7/\2\2\u00b0\u00b2")
        buf.write("\7@\2\2\u00b1\u00ac\3\2\2\2\u00b1\u00af\3\2\2\2\u00b2")
        buf.write("\66\3\2\2\2\u00b3\u00b4\7V\2\2\u00b4\u00b5\7t\2\2\u00b5")
        buf.write("\u00b6\7w\2\2\u00b6\u00c0\7g\2\2\u00b7\u00b8\7V\2\2\u00b8")
        buf.write("\u00b9\7T\2\2\u00b9\u00ba\7W\2\2\u00ba\u00c0\7G\2\2\u00bb")
        buf.write("\u00bc\7v\2\2\u00bc\u00bd\7t\2\2\u00bd\u00be\7w\2\2\u00be")
        buf.write("\u00c0\7g\2\2\u00bf\u00b3\3\2\2\2\u00bf\u00b7\3\2\2\2")
        buf.write("\u00bf\u00bb\3\2\2\2\u00c08\3\2\2\2\u00c1\u00c2\7H\2\2")
        buf.write("\u00c2\u00c3\7c\2\2\u00c3\u00c4\7n\2\2\u00c4\u00c5\7u")
        buf.write("\2\2\u00c5\u00d1\7g\2\2\u00c6\u00c7\7H\2\2\u00c7\u00c8")
        buf.write("\7C\2\2\u00c8\u00c9\7N\2\2\u00c9\u00ca\7U\2\2\u00ca\u00d1")
        buf.write("\7G\2\2\u00cb\u00cc\7h\2\2\u00cc\u00cd\7c\2\2\u00cd\u00ce")
        buf.write("\7n\2\2\u00ce\u00cf\7u\2\2\u00cf\u00d1\7g\2\2\u00d0\u00c1")
        buf.write("\3\2\2\2\u00d0\u00c6\3\2\2\2\u00d0\u00cb\3\2\2\2\u00d1")
        buf.write(":\3\2\2\2\u00d2\u00d6\t\5\2\2\u00d3\u00d5\t\6\2\2\u00d4")
        buf.write("\u00d3\3\2\2\2\u00d5\u00d8\3\2\2\2\u00d6\u00d4\3\2\2\2")
        buf.write("\u00d6\u00d7\3\2\2\2\u00d7<\3\2\2\2\u00d8\u00d6\3\2\2")
        buf.write("\2\u00d9\u00dd\7e\2\2\u00da\u00dc\13\2\2\2\u00db\u00da")
        buf.write("\3\2\2\2\u00dc\u00df\3\2\2\2\u00dd\u00de\3\2\2\2\u00dd")
        buf.write("\u00db\3\2\2\2\u00de\u00e0\3\2\2\2\u00df\u00dd\3\2\2\2")
        buf.write("\u00e0\u00e1\5C\"\2\u00e1\u00e2\3\2\2\2\u00e2\u00e3\b")
        buf.write("\37\2\2\u00e3>\3\2\2\2\u00e4\u00e5\t\7\2\2\u00e5@\3\2")
        buf.write("\2\2\u00e6\u00e8\t\b\2\2\u00e7\u00e6\3\2\2\2\u00e8\u00e9")
        buf.write("\3\2\2\2\u00e9\u00e7\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea")
        buf.write("\u00eb\3\2\2\2\u00eb\u00ec\b!\2\2\u00ecB\3\2\2\2\u00ed")
        buf.write("\u00ef\7\17\2\2\u00ee\u00ed\3\2\2\2\u00ee\u00ef\3\2\2")
        buf.write("\2\u00ef\u00f0\3\2\2\2\u00f0\u00f1\7\f\2\2\u00f1D\3\2")
        buf.write("\2\2\16\2\u0096\u009c\u00a1\u00aa\u00b1\u00bf\u00d0\u00d6")
        buf.write("\u00dd\u00e9\u00ee\3\b\2\2")
        return buf.getvalue()


class SlsgInputLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    HEADER = 21
    NUMBER = 22
    OP_AND = 23
    OP_OR = 24
    OP_NOT = 25
    OP_IMP = 26
    TRUE = 27
    FALSE = 28
    STRAT_VAR = 29
    COMMENT = 30
    VAL = 31
    WS = 32
    EOL = 33

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'kagent'", "'kslsg'", "'F'", "'G'", "'X'", "'('", "'U'", "')'", 
            "'R'", "'.'", "'['", "']'", "'E'", "'A'", "'<'", "','", "'>'", 
            "'kval'", "'kprot'", "'ktrans'" ]

    symbolicNames = [ "<INVALID>",
            "HEADER", "NUMBER", "OP_AND", "OP_OR", "OP_NOT", "OP_IMP", "TRUE", 
            "FALSE", "STRAT_VAR", "COMMENT", "VAL", "WS", "EOL" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "T__15", "T__16", "T__17", "T__18", "T__19", 
                  "HEADER", "NUMBER", "OP_AND", "OP_OR", "OP_NOT", "OP_IMP", 
                  "TRUE", "FALSE", "STRAT_VAR", "COMMENT", "VAL", "WS", 
                  "EOL" ]

    grammarFileName = "SlsgInput.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


