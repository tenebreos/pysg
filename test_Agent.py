#!/usr/bin/python3

# tests/Agent: test the agent and multi agent system representations.

import unittest

from Agent import MultiAgentSystem, AgentConstraint


class MasTests(unittest.TestCase):
    def setUp(self):
        AgentsConstraints = []
        AgentsConstraints.append(AgentConstraint(3, 3, 1))
        AgentsConstraints.append(AgentConstraint(2, 2, 1))
        Mas = MultiAgentSystem()
        for c in AgentsConstraints:
            Mas.AddAgent(c)
        Mas.SetInitialState(0, 0)
        Mas.ComputeModel()
        self.mas = Mas
        self.expectedJointActions = [
            (0, 0),
            (0, 1),
            (1, 0),
            (1, 1),
            (2, 0),
            (2, 1)
        ]
        self.expectedGlobalStates = self.expectedJointActions

    def test___GenerateJointActions(self):
        for i, action in enumerate(self.mas.jointActions):
            self.assertEqual(action, self.expectedJointActions[i])

    def test_JointActionNumber2Tuple_works_as_expected(self):
        for actionNumber, actionTuple in enumerate(self.expectedJointActions):
            a = self.mas.JointActionNumber2Tuple(actionNumber)
            self.assertEqual(a, actionTuple)

    def test_JointActionTuple2Number_works_as_expected(self):
        for actionNumber, actionTuple in enumerate(self.expectedJointActions):
            j = self.mas.JointActionTuple2Number(actionTuple)
            self.assertEqual(j, actionNumber)

    def test_JoinAction_conversion_round_trip(self):
        for actionNumber, actionTuple in enumerate(self.expectedJointActions):
            a = self.mas.JointActionNumber2Tuple(actionNumber)
            j = self.mas.JointActionTuple2Number(a)
            self.assertEqual(j, actionNumber)

    def test___GenerateGlobalStates(self):
        for i, state in enumerate(self.mas.globalStates):
            self.assertEqual(state, self.expectedGlobalStates[i])

    def test_GlobalStateNumber2Tuple_works_as_expected(self):
        for stateNumber, stateTuple in enumerate(self.expectedGlobalStates):
            a = self.mas.GlobalStateNumber2Tuple(stateNumber)
            self.assertEqual(a, stateTuple)

    def test_GlobalStateTuple2Number_works_as_expected(self):
        for stateNumber, stateTuple in enumerate(self.expectedGlobalStates):
            t = self.mas.GlobalStateTuple2Number(stateTuple)
            self.assertEqual(t, stateNumber)

    def test_GlobalState_conversion_round_trip(self):
        for stateNumber, stateTuple in enumerate(self.expectedGlobalStates):
            a = self.mas.JointActionNumber2Tuple(stateNumber)
            j = self.mas.JointActionTuple2Number(a)
            self.assertEqual(j, stateNumber)

    def test_SetInitialState_works_as_expected(self):
        self.mas.SetInitialState(2, 0)
        self.assertEqual(self.mas.initialState, 4)

    def test_AddAgent_adds_agent(self):
        mas = MultiAgentSystem()
        n_agents = len(mas.agents)
        mas.AddAgent(AgentConstraint(3, 3, 3))
        self.assertEqual(len(mas.agents), n_agents + 1)

    def test_CorrespondingJointActions_works_as_expected(self):
        expectedOutputs = [
            [
                [0, 1],
                [2, 3],
                [4, 5]
            ],
            [
                [0, 2, 4],
                [1, 3, 5]
            ]
        ]
        for i in range(len(self.mas.agents)):
            for a in range(self.mas.agents[i].constraints.actionsCount):
                output = self.mas.CorrespondingJointActions(a, i)
                self.assertEqual(expectedOutputs[i][a], output)

    def test_CorrespondingJointActions_errors_on_invalid_action_number(self):
        with self.assertRaises(ValueError):
            i = 0
            t = self.mas.agents[0].constraints.actionsCount + 21
            self.mas.CorrespondingJointActions(i, t)

    def test_CorrespondingJointActions_errors_on_invalid_action_index(self):
        with self.assertRaises(ValueError):
            i = len(self.mas.agents) + 21
            self.mas.CorrespondingJointActions(i, 0)

    def test_CorrespondingGlobalStates_works_as_expected(self):
        expectedOutputs = [
            [
                [0, 1],
                [2, 3],
                [4, 5]
            ],
            [
                [0, 2, 4],
                [1, 3, 5]
            ]
        ]
        for i in range(len(self.mas.agents)):
            for a in range(self.mas.agents[i].constraints.localStatesCount):
                output = self.mas.CorrespondingGlobalStates(a, i)
                self.assertEqual(expectedOutputs[i][a], output)

    def test_CorrespondingGlobalStates_errors_on_invalid_action_number(self):
        with self.assertRaises(ValueError):
            i = 0
            t = self.mas.agents[0].constraints.actionsCount + 21
            self.mas.CorrespondingGlobalStates(i, t)

    def test_CorrespondingGlobalStates_errors_on_invalid_action_index(self):
        with self.assertRaises(ValueError):
            i = len(self.mas.agents) + 21
            self.mas.CorrespondingGlobalStates(i, 0)
