#!/usr/bin/python3

from z3 import And, Or, Implies, Not


def Phi(mas):
    return And(Phi1(mas), Phi2(mas), Phi3(mas))


def Phi1(mas):
    alpha = []
    for agent in mas.agents:
        pb = agent.pb
        beta = []
        for k in range(agent.constraints.localStatesCount):
            gamma = []
            for t in range(agent.constraints.actionsCount):
                gamma.append(pb[k][t])
            beta.append(Or(gamma))
        alpha.append(And(beta))
    phi1 = And(alpha)
    return phi1


def Phi2(mas):
    alpha = []
    for agent in mas.agents:
        tb = agent.tb
        pb = agent.pb
        beta = []
        for t in range(agent.constraints.actionsCount):
            gamma = []
            for k in range(agent.constraints.localStatesCount):
                delta = []
                for j in mas.CorrespondingJointActions(t, agent.index):
                    epsilon = []
                    for kp in range(agent.constraints.localStatesCount):
                        epsilon.append(tb[k][j][kp])
                    delta.append(Or(epsilon) == pb[k][t])
                gamma.append(And(delta))
            beta.append(And(gamma))
        alpha.append(And(beta))
    phi2 = And(alpha)
    return phi2


def Phi3(mas):
    ActSize = len(mas.jointActions)
    alpha = []
    for agent in mas.agents:
        tb = agent.tb
        beta = []
        for k in range(agent.constraints.localStatesCount):
            gamma = []
            for kp in range(agent.constraints.localStatesCount):
                # kp is k'.
                delta = []
                for j in range(ActSize):
                    epsilon = []
                    for kdp in range(agent.constraints.localStatesCount):
                        # kdp is k''.
                        if kdp != kp:
                            epsilon.append(Not(tb[k][j][kdp]))
                    delta.append(Implies(tb[k][j][kp],
                                         And(epsilon)))
                gamma.append(And(delta))
            beta.append(And(gamma))
        alpha.append(And(beta))
    phi3 = And(alpha)
    return phi3
