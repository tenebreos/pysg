#!/usr/bin/python3

import unittest
from z3 import And, Or, Implies, Not

import Agent
import Phi


class TestPhi(unittest.TestCase):
    def setUp(self):
        AgentsConstraints = []
        AgentsConstraints.append(Agent.AgentConstraint(3, 2, 1))
        AgentsConstraints.append(Agent.AgentConstraint(2, 2, 1))
        Mas = Agent.MultiAgentSystem()
        for c in AgentsConstraints:
            Mas.AddAgent(c)
        Mas.SetInitialState(0, 0)
        Mas.ComputeModel()
        self.mas = Mas
        a = Mas.agents
        self.expectedPhi1 = And(And(Or(a[0].pb[0][0], a[0].pb[0][1]),
                                    Or(a[0].pb[1][0], a[0].pb[1][1]),
                                    Or(a[0].pb[2][0], a[0].pb[2][1])),
                                And(Or(a[1].pb[0][0], a[1].pb[0][1]),
                                    Or(a[1].pb[1][0], a[1].pb[1][1])))
        self.expectedPhi2 = (
            And(And(And(And(Or(a[0].tb[0][0][0],
                               a[0].tb[0][0][1],
                               a[0].tb[0][0][2]) == a[0].pb[0][0],
                            Or(a[0].tb[0][1][0],
                               a[0].tb[0][1][1],
                               a[0].tb[0][1][2]) == a[0].pb[0][0]),
                        And(Or(a[0].tb[1][0][0],
                               a[0].tb[1][0][1],
                               a[0].tb[1][0][2]) == a[0].pb[1][0],
                            Or(a[0].tb[1][1][0],
                               a[0].tb[1][1][1],
                               a[0].tb[1][1][2]) == a[0].pb[1][0]),
                        And(Or(a[0].tb[2][0][0],
                               a[0].tb[2][0][1],
                               a[0].tb[2][0][2]) == a[0].pb[2][0],
                            Or(a[0].tb[2][1][0],
                               a[0].tb[2][1][1],
                               a[0].tb[2][1][2]) == a[0].pb[2][0])),
                    And(And(Or(a[0].tb[0][2][0],
                               a[0].tb[0][2][1],
                               a[0].tb[0][2][2]) == a[0].pb[0][1],
                            Or(a[0].tb[0][3][0],
                               a[0].tb[0][3][1],
                               a[0].tb[0][3][2]) == a[0].pb[0][1]),
                        And(Or(a[0].tb[1][2][0],
                               a[0].tb[1][2][1],
                               a[0].tb[1][2][2]) == a[0].pb[1][1],
                            Or(a[0].tb[1][3][0],
                               a[0].tb[1][3][1],
                               a[0].tb[1][3][2]) == a[0].pb[1][1]),
                        And(Or(a[0].tb[2][2][0],
                               a[0].tb[2][2][1],
                               a[0].tb[2][2][2]) == a[0].pb[2][1],
                            Or(a[0].tb[2][3][0],
                               a[0].tb[2][3][1],
                               a[0].tb[2][3][2]) == a[0].pb[2][1]))),
                And(And(And(Or(a[1].tb[0][0][0],
                               a[1].tb[0][0][1]) == a[1].pb[0][0],
                            Or(a[1].tb[0][2][0],
                               a[1].tb[0][2][1]) == a[1].pb[0][0]),
                        And(Or(a[1].tb[1][0][0],
                               a[1].tb[1][0][1]) == a[1].pb[1][0],
                            Or(a[1].tb[1][2][0],
                               a[1].tb[1][2][1]) == a[1].pb[1][0])),
                    And(And(Or(a[1].tb[0][1][0],
                               a[1].tb[0][1][1]) == a[1].pb[0][1],
                            Or(a[1].tb[0][3][0],
                               a[1].tb[0][3][1]) == a[1].pb[0][1]),
                        And(Or(a[1].tb[1][1][0],
                               a[1].tb[1][1][1]) == a[1].pb[1][1],
                            Or(a[1].tb[1][3][0],
                               a[1].tb[1][3][1]) == a[1].pb[1][1]))))
        )
        self.expectedPhi3 = And(
            And(And(And(Implies(a[0].tb[0][0][0],
                                And(Not(a[0].tb[0][0][1]),
                                    Not(a[0].tb[0][0][2]))),
                        Implies(a[0].tb[0][1][0],
                                And(Not(a[0].tb[0][1][1]),
                                    Not(a[0].tb[0][1][2]))),
                        Implies(a[0].tb[0][2][0],
                                And(Not(a[0].tb[0][2][1]),
                                    Not(a[0].tb[0][2][2]))),
                        Implies(a[0].tb[0][3][0],
                                And(Not(a[0].tb[0][3][1]),
                                    Not(a[0].tb[0][3][2])))),
                    And(Implies(a[0].tb[0][0][1],
                                And(Not(a[0].tb[0][0][0]),
                                    Not(a[0].tb[0][0][2]))),
                        Implies(a[0].tb[0][1][1],
                                And(Not(a[0].tb[0][1][0]),
                                    Not(a[0].tb[0][1][2]))),
                        Implies(a[0].tb[0][2][1],
                                And(Not(a[0].tb[0][2][0]),
                                    Not(a[0].tb[0][2][2]))),
                        Implies(a[0].tb[0][3][1],
                                And(Not(a[0].tb[0][3][0]),
                                    Not(a[0].tb[0][3][2])))),
                    And(Implies(a[0].tb[0][0][2],
                                And(Not(a[0].tb[0][0][0]),
                                    Not(a[0].tb[0][0][1]))),
                        Implies(a[0].tb[0][1][2],
                                And(Not(a[0].tb[0][1][0]),
                                    Not(a[0].tb[0][1][1]))),
                        Implies(a[0].tb[0][2][2],
                                And(Not(a[0].tb[0][2][0]),
                                    Not(a[0].tb[0][2][1]))),
                        Implies(a[0].tb[0][3][2],
                                And(Not(a[0].tb[0][3][0]),
                                    Not(a[0].tb[0][3][1]))))),
                And(And(Implies(a[0].tb[1][0][0],
                                And(Not(a[0].tb[1][0][1]),
                                    Not(a[0].tb[1][0][2]))),
                        Implies(a[0].tb[1][1][0],
                                And(Not(a[0].tb[1][1][1]),
                                    Not(a[0].tb[1][1][2]))),
                        Implies(a[0].tb[1][2][0],
                                And(Not(a[0].tb[1][2][1]),
                                    Not(a[0].tb[1][2][2]))),
                        Implies(a[0].tb[1][3][0],
                                And(Not(a[0].tb[1][3][1]),
                                    Not(a[0].tb[1][3][2])))),
                    And(Implies(a[0].tb[1][0][1],
                                And(Not(a[0].tb[1][0][0]),
                                    Not(a[0].tb[1][0][2]))),
                        Implies(a[0].tb[1][1][1],
                                And(Not(a[0].tb[1][1][0]),
                                    Not(a[0].tb[1][1][2]))),
                        Implies(a[0].tb[1][2][1],
                                And(Not(a[0].tb[1][2][0]),
                                    Not(a[0].tb[1][2][2]))),
                        Implies(a[0].tb[1][3][1],
                                And(Not(a[0].tb[1][3][0]),
                                    Not(a[0].tb[1][3][2])))),
                    And(Implies(a[0].tb[1][0][2],
                                And(Not(a[0].tb[1][0][0]),
                                    Not(a[0].tb[1][0][1]))),
                        Implies(a[0].tb[1][1][2],
                                And(Not(a[0].tb[1][1][0]),
                                    Not(a[0].tb[1][1][1]))),
                        Implies(a[0].tb[1][2][2],
                                And(Not(a[0].tb[1][2][0]),
                                    Not(a[0].tb[1][2][1]))),
                        Implies(a[0].tb[1][3][2],
                                And(Not(a[0].tb[1][3][0]),
                                    Not(a[0].tb[1][3][1]))))),
                And(And(Implies(a[0].tb[2][0][0],
                                And(Not(a[0].tb[2][0][1]),
                                    Not(a[0].tb[2][0][2]))),
                        Implies(a[0].tb[2][1][0],
                                And(Not(a[0].tb[2][1][1]),
                                    Not(a[0].tb[2][1][2]))),
                        Implies(a[0].tb[2][2][0],
                                And(Not(a[0].tb[2][2][1]),
                                    Not(a[0].tb[2][2][2]))),
                        Implies(a[0].tb[2][3][0],
                                And(Not(a[0].tb[2][3][1]),
                                    Not(a[0].tb[2][3][2])))),
                    And(Implies(a[0].tb[2][0][1],
                                And(Not(a[0].tb[2][0][0]),
                                    Not(a[0].tb[2][0][2]))),
                        Implies(a[0].tb[2][1][1],
                                And(Not(a[0].tb[2][1][0]),
                                    Not(a[0].tb[2][1][2]))),
                        Implies(a[0].tb[2][2][1],
                                And(Not(a[0].tb[2][2][0]),
                                    Not(a[0].tb[2][2][2]))),
                        Implies(a[0].tb[2][3][1],
                                And(Not(a[0].tb[2][3][0]),
                                    Not(a[0].tb[2][3][2])))),
                    And(Implies(a[0].tb[2][0][2],
                                And(Not(a[0].tb[2][0][0]),
                                    Not(a[0].tb[2][0][1]))),
                        Implies(a[0].tb[2][1][2],
                                And(Not(a[0].tb[2][1][0]),
                                    Not(a[0].tb[2][1][1]))),
                        Implies(a[0].tb[2][2][2],
                                And(Not(a[0].tb[2][2][0]),
                                    Not(a[0].tb[2][2][1]))),
                        Implies(a[0].tb[2][3][2],
                                And(Not(a[0].tb[2][3][0]),
                                    Not(a[0].tb[2][3][1])))))),
            And(And(And(Implies(a[1].tb[0][0][0], And(Not(a[1].tb[0][0][1]))),
                        Implies(a[1].tb[0][1][0], And(Not(a[1].tb[0][1][1]))),
                        Implies(a[1].tb[0][2][0], And(Not(a[1].tb[0][2][1]))),
                        Implies(a[1].tb[0][3][0], And(Not(a[1].tb[0][3][1])))),
                    And(Implies(a[1].tb[0][0][1], And(Not(a[1].tb[0][0][0]))),
                        Implies(a[1].tb[0][1][1], And(Not(a[1].tb[0][1][0]))),
                        Implies(a[1].tb[0][2][1], And(Not(a[1].tb[0][2][0]))),
                        Implies(a[1].tb[0][3][1], And(Not(a[1].tb[0][3][0]))))),
                And(And(Implies(a[1].tb[1][0][0], And(Not(a[1].tb[1][0][1]))),
                        Implies(a[1].tb[1][1][0], And(Not(a[1].tb[1][1][1]))),
                        Implies(a[1].tb[1][2][0], And(Not(a[1].tb[1][2][1]))),
                        Implies(a[1].tb[1][3][0], And(Not(a[1].tb[1][3][1])))),
                    And(Implies(a[1].tb[1][0][1], And(Not(a[1].tb[1][0][0]))),
                        Implies(a[1].tb[1][1][1], And(Not(a[1].tb[1][1][0]))),
                        Implies(a[1].tb[1][2][1], And(Not(a[1].tb[1][2][0]))),
                        Implies(a[1].tb[1][3][1], And(Not(a[1].tb[1][3][0])))))))
        self.expectedPhi = (And(self.expectedPhi1,
                                self.expectedPhi2,
                                self.expectedPhi3))

    def test_Phi1_works_as_expected(self):
        phi1 = Phi.Phi1(self.mas)
        self.assertEqual(self.expectedPhi1, phi1)

    def test_Phi2_works_as_expected(self):
        phi2 = Phi.Phi2(self.mas)
        self.assertEqual(self.expectedPhi2, phi2)

    def test_Phi3_works_as_expected(self):
        phi3 = Phi.Phi3(self.mas)
        self.assertEqual(self.expectedPhi3, phi3)

    def test_Phi_works_as_expected(self):
        phi = Phi.Phi(self.mas)
        self.assertEqual(self.expectedPhi, phi)
