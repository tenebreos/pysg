# Generated from SlsgInput.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3#")
        buf.write("\u00ca\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2")
        buf.write("\3\2\6\2\63\n\2\r\2\16\2\64\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\5\3A\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3")
        buf.write("\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3")
        buf.write("\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3")
        buf.write("\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n")
        buf.write("\u008d\n\n\3\13\3\13\3\13\3\13\3\f\3\f\3\r\6\r\u0096\n")
        buf.write("\r\r\r\16\r\u0097\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3")
        buf.write("\20\6\20\u00a2\n\20\r\20\16\20\u00a3\3\21\3\21\3\21\3")
        buf.write("\21\3\21\3\21\3\22\3\22\3\22\5\22\u00af\n\22\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\27\3\27\3\30")
        buf.write("\3\30\3\30\2\2\31\2\4\6\b\n\f\16\20\22\24\26\30\32\34")
        buf.write("\36 \"$&(*,.\2\3\3\2\17\20\2\u00c5\2\60\3\2\2\2\4@\3\2")
        buf.write("\2\2\6B\3\2\2\2\bH\3\2\2\2\nJ\3\2\2\2\fL\3\2\2\2\16N\3")
        buf.write("\2\2\2\20P\3\2\2\2\22\u008c\3\2\2\2\24\u008e\3\2\2\2\26")
        buf.write("\u0092\3\2\2\2\30\u0095\3\2\2\2\32\u0099\3\2\2\2\34\u009e")
        buf.write("\3\2\2\2\36\u00a1\3\2\2\2 \u00a5\3\2\2\2\"\u00ae\3\2\2")
        buf.write("\2$\u00b0\3\2\2\2&\u00b6\3\2\2\2(\u00bc\3\2\2\2*\u00c3")
        buf.write("\3\2\2\2,\u00c5\3\2\2\2.\u00c7\3\2\2\2\60\62\7\27\2\2")
        buf.write("\61\63\5\4\3\2\62\61\3\2\2\2\63\64\3\2\2\2\64\62\3\2\2")
        buf.write("\2\64\65\3\2\2\2\65\3\3\2\2\2\66\67\5\6\4\2\678\7#\2\2")
        buf.write("8A\3\2\2\29:\5\"\22\2:;\7#\2\2;A\3\2\2\2<=\5\20\t\2=>")
        buf.write("\7#\2\2>A\3\2\2\2?A\7#\2\2@\66\3\2\2\2@9\3\2\2\2@<\3\2")
        buf.write("\2\2@?\3\2\2\2A\5\3\2\2\2BC\7\3\2\2CD\5\b\5\2DE\5\n\6")
        buf.write("\2EF\5\f\7\2FG\5\16\b\2G\7\3\2\2\2HI\7\30\2\2I\t\3\2\2")
        buf.write("\2JK\7\30\2\2K\13\3\2\2\2LM\7\30\2\2M\r\3\2\2\2NO\7\30")
        buf.write("\2\2O\17\3\2\2\2PQ\7\4\2\2QR\5\22\n\2R\21\3\2\2\2ST\5")
        buf.write("\30\r\2TU\5\36\20\2UV\7\5\2\2VW\5\22\n\2W\u008d\3\2\2")
        buf.write("\2XY\5\30\r\2YZ\5\36\20\2Z[\7\6\2\2[\\\5\22\n\2\\\u008d")
        buf.write("\3\2\2\2]^\5\30\r\2^_\5\36\20\2_`\7\7\2\2`a\5\22\n\2a")
        buf.write("\u008d\3\2\2\2bc\5\30\r\2cd\5\36\20\2de\7\b\2\2ef\5\22")
        buf.write("\n\2fg\7\t\2\2gh\5\22\n\2hi\7\n\2\2i\u008d\3\2\2\2jk\5")
        buf.write("\30\r\2kl\5\36\20\2lm\7\b\2\2mn\5\22\n\2no\7\13\2\2op")
        buf.write("\5\22\n\2pq\7\n\2\2q\u008d\3\2\2\2rs\7\b\2\2st\5\22\n")
        buf.write("\2tu\7\31\2\2uv\5\22\n\2vw\7\n\2\2w\u008d\3\2\2\2xy\7")
        buf.write("\b\2\2yz\5\22\n\2z{\7\32\2\2{|\5\22\n\2|}\7\n\2\2}\u008d")
        buf.write("\3\2\2\2~\177\7\b\2\2\177\u0080\5\22\n\2\u0080\u0081\7")
        buf.write("\34\2\2\u0081\u0082\5\22\n\2\u0082\u0083\7\n\2\2\u0083")
        buf.write("\u008d\3\2\2\2\u0084\u0085\7\33\2\2\u0085\u0086\7\b\2")
        buf.write("\2\u0086\u0087\5\22\n\2\u0087\u0088\7\n\2\2\u0088\u008d")
        buf.write("\3\2\2\2\u0089\u008d\5\24\13\2\u008a\u008d\7\35\2\2\u008b")
        buf.write("\u008d\7\36\2\2\u008cS\3\2\2\2\u008cX\3\2\2\2\u008c]\3")
        buf.write("\2\2\2\u008cb\3\2\2\2\u008cj\3\2\2\2\u008cr\3\2\2\2\u008c")
        buf.write("x\3\2\2\2\u008c~\3\2\2\2\u008c\u0084\3\2\2\2\u008c\u0089")
        buf.write("\3\2\2\2\u008c\u008a\3\2\2\2\u008c\u008b\3\2\2\2\u008d")
        buf.write("\23\3\2\2\2\u008e\u008f\5\b\5\2\u008f\u0090\7\f\2\2\u0090")
        buf.write("\u0091\5\26\f\2\u0091\25\3\2\2\2\u0092\u0093\7\30\2\2")
        buf.write("\u0093\27\3\2\2\2\u0094\u0096\5\32\16\2\u0095\u0094\3")
        buf.write("\2\2\2\u0096\u0097\3\2\2\2\u0097\u0095\3\2\2\2\u0097\u0098")
        buf.write("\3\2\2\2\u0098\31\3\2\2\2\u0099\u009a\7\r\2\2\u009a\u009b")
        buf.write("\5\34\17\2\u009b\u009c\7\37\2\2\u009c\u009d\7\16\2\2\u009d")
        buf.write("\33\3\2\2\2\u009e\u009f\t\2\2\2\u009f\35\3\2\2\2\u00a0")
        buf.write("\u00a2\5 \21\2\u00a1\u00a0\3\2\2\2\u00a2\u00a3\3\2\2\2")
        buf.write("\u00a3\u00a1\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\37\3\2")
        buf.write("\2\2\u00a5\u00a6\7\21\2\2\u00a6\u00a7\7\37\2\2\u00a7\u00a8")
        buf.write("\7\22\2\2\u00a8\u00a9\5\b\5\2\u00a9\u00aa\7\23\2\2\u00aa")
        buf.write("!\3\2\2\2\u00ab\u00af\5$\23\2\u00ac\u00af\5&\24\2\u00ad")
        buf.write("\u00af\5(\25\2\u00ae\u00ab\3\2\2\2\u00ae\u00ac\3\2\2\2")
        buf.write("\u00ae\u00ad\3\2\2\2\u00af#\3\2\2\2\u00b0\u00b1\7\24\2")
        buf.write("\2\u00b1\u00b2\7!\2\2\u00b2\u00b3\5\b\5\2\u00b3\u00b4")
        buf.write("\5*\26\2\u00b4\u00b5\5\26\f\2\u00b5%\3\2\2\2\u00b6\u00b7")
        buf.write("\7\25\2\2\u00b7\u00b8\7!\2\2\u00b8\u00b9\5\b\5\2\u00b9")
        buf.write("\u00ba\5*\26\2\u00ba\u00bb\5,\27\2\u00bb\'\3\2\2\2\u00bc")
        buf.write("\u00bd\7\26\2\2\u00bd\u00be\7!\2\2\u00be\u00bf\5\b\5\2")
        buf.write("\u00bf\u00c0\5*\26\2\u00c0\u00c1\5.\30\2\u00c1\u00c2\5")
        buf.write("*\26\2\u00c2)\3\2\2\2\u00c3\u00c4\7\30\2\2\u00c4+\3\2")
        buf.write("\2\2\u00c5\u00c6\7\30\2\2\u00c6-\3\2\2\2\u00c7\u00c8\7")
        buf.write("\30\2\2\u00c8/\3\2\2\2\b\64@\u008c\u0097\u00a3\u00ae")
        return buf.getvalue()


class SlsgInputParser ( Parser ):

    grammarFileName = "SlsgInput.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'kagent'", "'kslsg'", "'F'", "'G'", "'X'", 
                     "'('", "'U'", "')'", "'R'", "'.'", "'['", "']'", "'E'", 
                     "'A'", "'<'", "','", "'>'", "'kval'", "'kprot'", "'ktrans'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "HEADER", "NUMBER", "OP_AND", "OP_OR", 
                      "OP_NOT", "OP_IMP", "TRUE", "FALSE", "STRAT_VAR", 
                      "COMMENT", "VAL", "WS", "EOL" ]

    RULE_input_file = 0
    RULE_line = 1
    RULE_agent_spec = 2
    RULE_agent_id = 3
    RULE_local_states_no = 4
    RULE_local_actions_no = 5
    RULE_local_props_no = 6
    RULE_formula = 7
    RULE_slsg_formula = 8
    RULE_prop = 9
    RULE_prop_id = 10
    RULE_quant_pref = 11
    RULE_quant = 12
    RULE_quantifier = 13
    RULE_bind_pref = 14
    RULE_bind = 15
    RULE_override = 16
    RULE_val_override = 17
    RULE_prot_override = 18
    RULE_trans_override = 19
    RULE_local_state_id = 20
    RULE_local_action_id = 21
    RULE_joint_action_id = 22

    ruleNames =  [ "input_file", "line", "agent_spec", "agent_id", "local_states_no", 
                   "local_actions_no", "local_props_no", "formula", "slsg_formula", 
                   "prop", "prop_id", "quant_pref", "quant", "quantifier", 
                   "bind_pref", "bind", "override", "val_override", "prot_override", 
                   "trans_override", "local_state_id", "local_action_id", 
                   "joint_action_id" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    HEADER=21
    NUMBER=22
    OP_AND=23
    OP_OR=24
    OP_NOT=25
    OP_IMP=26
    TRUE=27
    FALSE=28
    STRAT_VAR=29
    COMMENT=30
    VAL=31
    WS=32
    EOL=33

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class Input_fileContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HEADER(self):
            return self.getToken(SlsgInputParser.HEADER, 0)

        def line(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.LineContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.LineContext,i)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_input_file

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInput_file" ):
                listener.enterInput_file(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInput_file" ):
                listener.exitInput_file(self)




    def input_file(self):

        localctx = SlsgInputParser.Input_fileContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_input_file)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            self.match(SlsgInputParser.HEADER)
            self.state = 48 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 47
                self.line()
                self.state = 50 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << SlsgInputParser.T__0) | (1 << SlsgInputParser.T__1) | (1 << SlsgInputParser.T__17) | (1 << SlsgInputParser.T__18) | (1 << SlsgInputParser.T__19) | (1 << SlsgInputParser.EOL))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LineContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def agent_spec(self):
            return self.getTypedRuleContext(SlsgInputParser.Agent_specContext,0)


        def EOL(self):
            return self.getToken(SlsgInputParser.EOL, 0)

        def override(self):
            return self.getTypedRuleContext(SlsgInputParser.OverrideContext,0)


        def formula(self):
            return self.getTypedRuleContext(SlsgInputParser.FormulaContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_line

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLine" ):
                listener.enterLine(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLine" ):
                listener.exitLine(self)




    def line(self):

        localctx = SlsgInputParser.LineContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_line)
        try:
            self.state = 62
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SlsgInputParser.T__0]:
                self.enterOuterAlt(localctx, 1)
                self.state = 52
                self.agent_spec()
                self.state = 53
                self.match(SlsgInputParser.EOL)
                pass
            elif token in [SlsgInputParser.T__17, SlsgInputParser.T__18, SlsgInputParser.T__19]:
                self.enterOuterAlt(localctx, 2)
                self.state = 55
                self.override()
                self.state = 56
                self.match(SlsgInputParser.EOL)
                pass
            elif token in [SlsgInputParser.T__1]:
                self.enterOuterAlt(localctx, 3)
                self.state = 58
                self.formula()
                self.state = 59
                self.match(SlsgInputParser.EOL)
                pass
            elif token in [SlsgInputParser.EOL]:
                self.enterOuterAlt(localctx, 4)
                self.state = 61
                self.match(SlsgInputParser.EOL)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Agent_specContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def agent_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Agent_idContext,0)


        def local_states_no(self):
            return self.getTypedRuleContext(SlsgInputParser.Local_states_noContext,0)


        def local_actions_no(self):
            return self.getTypedRuleContext(SlsgInputParser.Local_actions_noContext,0)


        def local_props_no(self):
            return self.getTypedRuleContext(SlsgInputParser.Local_props_noContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_agent_spec

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAgent_spec" ):
                listener.enterAgent_spec(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAgent_spec" ):
                listener.exitAgent_spec(self)




    def agent_spec(self):

        localctx = SlsgInputParser.Agent_specContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_agent_spec)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self.match(SlsgInputParser.T__0)
            self.state = 65
            self.agent_id()
            self.state = 66
            self.local_states_no()
            self.state = 67
            self.local_actions_no()
            self.state = 68
            self.local_props_no()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Agent_idContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_agent_id

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAgent_id" ):
                listener.enterAgent_id(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAgent_id" ):
                listener.exitAgent_id(self)




    def agent_id(self):

        localctx = SlsgInputParser.Agent_idContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_agent_id)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Local_states_noContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_local_states_no

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLocal_states_no" ):
                listener.enterLocal_states_no(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLocal_states_no" ):
                listener.exitLocal_states_no(self)




    def local_states_no(self):

        localctx = SlsgInputParser.Local_states_noContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_local_states_no)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 72
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Local_actions_noContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_local_actions_no

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLocal_actions_no" ):
                listener.enterLocal_actions_no(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLocal_actions_no" ):
                listener.exitLocal_actions_no(self)




    def local_actions_no(self):

        localctx = SlsgInputParser.Local_actions_noContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_local_actions_no)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Local_props_noContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_local_props_no

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLocal_props_no" ):
                listener.enterLocal_props_no(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLocal_props_no" ):
                listener.exitLocal_props_no(self)




    def local_props_no(self):

        localctx = SlsgInputParser.Local_props_noContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_local_props_no)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 76
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FormulaContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def slsg_formula(self):
            return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_formula

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormula" ):
                listener.enterFormula(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormula" ):
                listener.exitFormula(self)




    def formula(self):

        localctx = SlsgInputParser.FormulaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_formula)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 78
            self.match(SlsgInputParser.T__1)
            self.state = 79
            self.slsg_formula()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Slsg_formulaContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SlsgInputParser.RULE_slsg_formula

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class FormulaFalseContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def FALSE(self):
            return self.getToken(SlsgInputParser.FALSE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaFalse" ):
                listener.enterFormulaFalse(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaFalse" ):
                listener.exitFormulaFalse(self)


    class FormulaPropContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def prop(self):
            return self.getTypedRuleContext(SlsgInputParser.PropContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaProp" ):
                listener.enterFormulaProp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaProp" ):
                listener.exitFormulaProp(self)


    class FormulaNextContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quant_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Quant_prefContext,0)

        def bind_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Bind_prefContext,0)

        def slsg_formula(self):
            return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaNext" ):
                listener.enterFormulaNext(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaNext" ):
                listener.exitFormulaNext(self)


    class FormulaAndContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def slsg_formula(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.Slsg_formulaContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,i)

        def OP_AND(self):
            return self.getToken(SlsgInputParser.OP_AND, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaAnd" ):
                listener.enterFormulaAnd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaAnd" ):
                listener.exitFormulaAnd(self)


    class FormulaNotContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def OP_NOT(self):
            return self.getToken(SlsgInputParser.OP_NOT, 0)
        def slsg_formula(self):
            return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaNot" ):
                listener.enterFormulaNot(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaNot" ):
                listener.exitFormulaNot(self)


    class FormulaTrueContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def TRUE(self):
            return self.getToken(SlsgInputParser.TRUE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaTrue" ):
                listener.enterFormulaTrue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaTrue" ):
                listener.exitFormulaTrue(self)


    class FormulaFutureContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quant_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Quant_prefContext,0)

        def bind_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Bind_prefContext,0)

        def slsg_formula(self):
            return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaFuture" ):
                listener.enterFormulaFuture(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaFuture" ):
                listener.exitFormulaFuture(self)


    class FormulaReleaseContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quant_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Quant_prefContext,0)

        def bind_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Bind_prefContext,0)

        def slsg_formula(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.Slsg_formulaContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaRelease" ):
                listener.enterFormulaRelease(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaRelease" ):
                listener.exitFormulaRelease(self)


    class FormulaOrContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def slsg_formula(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.Slsg_formulaContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,i)

        def OP_OR(self):
            return self.getToken(SlsgInputParser.OP_OR, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaOr" ):
                listener.enterFormulaOr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaOr" ):
                listener.exitFormulaOr(self)


    class FormulaGlobalContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quant_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Quant_prefContext,0)

        def bind_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Bind_prefContext,0)

        def slsg_formula(self):
            return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaGlobal" ):
                listener.enterFormulaGlobal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaGlobal" ):
                listener.exitFormulaGlobal(self)


    class FormulaUntilContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quant_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Quant_prefContext,0)

        def bind_pref(self):
            return self.getTypedRuleContext(SlsgInputParser.Bind_prefContext,0)

        def slsg_formula(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.Slsg_formulaContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaUntil" ):
                listener.enterFormulaUntil(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaUntil" ):
                listener.exitFormulaUntil(self)


    class FormulaImplContext(Slsg_formulaContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SlsgInputParser.Slsg_formulaContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def slsg_formula(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.Slsg_formulaContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.Slsg_formulaContext,i)

        def OP_IMP(self):
            return self.getToken(SlsgInputParser.OP_IMP, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFormulaImpl" ):
                listener.enterFormulaImpl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFormulaImpl" ):
                listener.exitFormulaImpl(self)



    def slsg_formula(self):

        localctx = SlsgInputParser.Slsg_formulaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_slsg_formula)
        try:
            self.state = 138
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                localctx = SlsgInputParser.FormulaFutureContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 81
                self.quant_pref()
                self.state = 82
                self.bind_pref()
                self.state = 83
                self.match(SlsgInputParser.T__2)
                self.state = 84
                self.slsg_formula()
                pass

            elif la_ == 2:
                localctx = SlsgInputParser.FormulaGlobalContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 86
                self.quant_pref()
                self.state = 87
                self.bind_pref()
                self.state = 88
                self.match(SlsgInputParser.T__3)
                self.state = 89
                self.slsg_formula()
                pass

            elif la_ == 3:
                localctx = SlsgInputParser.FormulaNextContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 91
                self.quant_pref()
                self.state = 92
                self.bind_pref()
                self.state = 93
                self.match(SlsgInputParser.T__4)
                self.state = 94
                self.slsg_formula()
                pass

            elif la_ == 4:
                localctx = SlsgInputParser.FormulaUntilContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 96
                self.quant_pref()
                self.state = 97
                self.bind_pref()
                self.state = 98
                self.match(SlsgInputParser.T__5)
                self.state = 99
                self.slsg_formula()
                self.state = 100
                self.match(SlsgInputParser.T__6)
                self.state = 101
                self.slsg_formula()
                self.state = 102
                self.match(SlsgInputParser.T__7)
                pass

            elif la_ == 5:
                localctx = SlsgInputParser.FormulaReleaseContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 104
                self.quant_pref()
                self.state = 105
                self.bind_pref()
                self.state = 106
                self.match(SlsgInputParser.T__5)
                self.state = 107
                self.slsg_formula()
                self.state = 108
                self.match(SlsgInputParser.T__8)
                self.state = 109
                self.slsg_formula()
                self.state = 110
                self.match(SlsgInputParser.T__7)
                pass

            elif la_ == 6:
                localctx = SlsgInputParser.FormulaAndContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 112
                self.match(SlsgInputParser.T__5)
                self.state = 113
                self.slsg_formula()
                self.state = 114
                self.match(SlsgInputParser.OP_AND)
                self.state = 115
                self.slsg_formula()
                self.state = 116
                self.match(SlsgInputParser.T__7)
                pass

            elif la_ == 7:
                localctx = SlsgInputParser.FormulaOrContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 118
                self.match(SlsgInputParser.T__5)
                self.state = 119
                self.slsg_formula()
                self.state = 120
                self.match(SlsgInputParser.OP_OR)
                self.state = 121
                self.slsg_formula()
                self.state = 122
                self.match(SlsgInputParser.T__7)
                pass

            elif la_ == 8:
                localctx = SlsgInputParser.FormulaImplContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 124
                self.match(SlsgInputParser.T__5)
                self.state = 125
                self.slsg_formula()
                self.state = 126
                self.match(SlsgInputParser.OP_IMP)
                self.state = 127
                self.slsg_formula()
                self.state = 128
                self.match(SlsgInputParser.T__7)
                pass

            elif la_ == 9:
                localctx = SlsgInputParser.FormulaNotContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 130
                self.match(SlsgInputParser.OP_NOT)
                self.state = 131
                self.match(SlsgInputParser.T__5)
                self.state = 132
                self.slsg_formula()
                self.state = 133
                self.match(SlsgInputParser.T__7)
                pass

            elif la_ == 10:
                localctx = SlsgInputParser.FormulaPropContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 135
                self.prop()
                pass

            elif la_ == 11:
                localctx = SlsgInputParser.FormulaTrueContext(self, localctx)
                self.enterOuterAlt(localctx, 11)
                self.state = 136
                self.match(SlsgInputParser.TRUE)
                pass

            elif la_ == 12:
                localctx = SlsgInputParser.FormulaFalseContext(self, localctx)
                self.enterOuterAlt(localctx, 12)
                self.state = 137
                self.match(SlsgInputParser.FALSE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PropContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def agent_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Agent_idContext,0)


        def prop_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Prop_idContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_prop

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProp" ):
                listener.enterProp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProp" ):
                listener.exitProp(self)




    def prop(self):

        localctx = SlsgInputParser.PropContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_prop)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 140
            self.agent_id()
            self.state = 141
            self.match(SlsgInputParser.T__9)
            self.state = 142
            self.prop_id()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Prop_idContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_prop_id

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProp_id" ):
                listener.enterProp_id(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProp_id" ):
                listener.exitProp_id(self)




    def prop_id(self):

        localctx = SlsgInputParser.Prop_idContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_prop_id)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Quant_prefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def quant(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.QuantContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.QuantContext,i)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_quant_pref

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuant_pref" ):
                listener.enterQuant_pref(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuant_pref" ):
                listener.exitQuant_pref(self)




    def quant_pref(self):

        localctx = SlsgInputParser.Quant_prefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_quant_pref)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 147 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 146
                self.quant()
                self.state = 149 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==SlsgInputParser.T__10):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class QuantContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def quantifier(self):
            return self.getTypedRuleContext(SlsgInputParser.QuantifierContext,0)


        def STRAT_VAR(self):
            return self.getToken(SlsgInputParser.STRAT_VAR, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_quant

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuant" ):
                listener.enterQuant(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuant" ):
                listener.exitQuant(self)




    def quant(self):

        localctx = SlsgInputParser.QuantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_quant)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 151
            self.match(SlsgInputParser.T__10)
            self.state = 152
            self.quantifier()
            self.state = 153
            self.match(SlsgInputParser.STRAT_VAR)
            self.state = 154
            self.match(SlsgInputParser.T__11)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class QuantifierContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SlsgInputParser.RULE_quantifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuantifier" ):
                listener.enterQuantifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuantifier" ):
                listener.exitQuantifier(self)




    def quantifier(self):

        localctx = SlsgInputParser.QuantifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_quantifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 156
            _la = self._input.LA(1)
            if not(_la==SlsgInputParser.T__12 or _la==SlsgInputParser.T__13):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bind_prefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def bind(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.BindContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.BindContext,i)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_bind_pref

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBind_pref" ):
                listener.enterBind_pref(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBind_pref" ):
                listener.exitBind_pref(self)




    def bind_pref(self):

        localctx = SlsgInputParser.Bind_prefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_bind_pref)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 159 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 158
                self.bind()
                self.state = 161 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==SlsgInputParser.T__14):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BindContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRAT_VAR(self):
            return self.getToken(SlsgInputParser.STRAT_VAR, 0)

        def agent_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Agent_idContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_bind

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBind" ):
                listener.enterBind(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBind" ):
                listener.exitBind(self)




    def bind(self):

        localctx = SlsgInputParser.BindContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_bind)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 163
            self.match(SlsgInputParser.T__14)
            self.state = 164
            self.match(SlsgInputParser.STRAT_VAR)
            self.state = 165
            self.match(SlsgInputParser.T__15)
            self.state = 166
            self.agent_id()
            self.state = 167
            self.match(SlsgInputParser.T__16)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OverrideContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def val_override(self):
            return self.getTypedRuleContext(SlsgInputParser.Val_overrideContext,0)


        def prot_override(self):
            return self.getTypedRuleContext(SlsgInputParser.Prot_overrideContext,0)


        def trans_override(self):
            return self.getTypedRuleContext(SlsgInputParser.Trans_overrideContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_override

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOverride" ):
                listener.enterOverride(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOverride" ):
                listener.exitOverride(self)




    def override(self):

        localctx = SlsgInputParser.OverrideContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_override)
        try:
            self.state = 172
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SlsgInputParser.T__17]:
                self.enterOuterAlt(localctx, 1)
                self.state = 169
                self.val_override()
                pass
            elif token in [SlsgInputParser.T__18]:
                self.enterOuterAlt(localctx, 2)
                self.state = 170
                self.prot_override()
                pass
            elif token in [SlsgInputParser.T__19]:
                self.enterOuterAlt(localctx, 3)
                self.state = 171
                self.trans_override()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Val_overrideContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAL(self):
            return self.getToken(SlsgInputParser.VAL, 0)

        def agent_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Agent_idContext,0)


        def local_state_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Local_state_idContext,0)


        def prop_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Prop_idContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_val_override

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVal_override" ):
                listener.enterVal_override(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVal_override" ):
                listener.exitVal_override(self)




    def val_override(self):

        localctx = SlsgInputParser.Val_overrideContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_val_override)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self.match(SlsgInputParser.T__17)
            self.state = 175
            self.match(SlsgInputParser.VAL)
            self.state = 176
            self.agent_id()
            self.state = 177
            self.local_state_id()
            self.state = 178
            self.prop_id()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Prot_overrideContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAL(self):
            return self.getToken(SlsgInputParser.VAL, 0)

        def agent_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Agent_idContext,0)


        def local_state_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Local_state_idContext,0)


        def local_action_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Local_action_idContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_prot_override

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProt_override" ):
                listener.enterProt_override(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProt_override" ):
                listener.exitProt_override(self)




    def prot_override(self):

        localctx = SlsgInputParser.Prot_overrideContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_prot_override)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 180
            self.match(SlsgInputParser.T__18)
            self.state = 181
            self.match(SlsgInputParser.VAL)
            self.state = 182
            self.agent_id()
            self.state = 183
            self.local_state_id()
            self.state = 184
            self.local_action_id()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Trans_overrideContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAL(self):
            return self.getToken(SlsgInputParser.VAL, 0)

        def agent_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Agent_idContext,0)


        def local_state_id(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SlsgInputParser.Local_state_idContext)
            else:
                return self.getTypedRuleContext(SlsgInputParser.Local_state_idContext,i)


        def joint_action_id(self):
            return self.getTypedRuleContext(SlsgInputParser.Joint_action_idContext,0)


        def getRuleIndex(self):
            return SlsgInputParser.RULE_trans_override

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTrans_override" ):
                listener.enterTrans_override(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTrans_override" ):
                listener.exitTrans_override(self)




    def trans_override(self):

        localctx = SlsgInputParser.Trans_overrideContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_trans_override)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 186
            self.match(SlsgInputParser.T__19)
            self.state = 187
            self.match(SlsgInputParser.VAL)
            self.state = 188
            self.agent_id()
            self.state = 189
            self.local_state_id()
            self.state = 190
            self.joint_action_id()
            self.state = 191
            self.local_state_id()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Local_state_idContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_local_state_id

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLocal_state_id" ):
                listener.enterLocal_state_id(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLocal_state_id" ):
                listener.exitLocal_state_id(self)




    def local_state_id(self):

        localctx = SlsgInputParser.Local_state_idContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_local_state_id)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 193
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Local_action_idContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_local_action_id

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLocal_action_id" ):
                listener.enterLocal_action_id(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLocal_action_id" ):
                listener.exitLocal_action_id(self)




    def local_action_id(self):

        localctx = SlsgInputParser.Local_action_idContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_local_action_id)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 195
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Joint_action_idContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(SlsgInputParser.NUMBER, 0)

        def getRuleIndex(self):
            return SlsgInputParser.RULE_joint_action_id

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterJoint_action_id" ):
                listener.enterJoint_action_id(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitJoint_action_id" ):
                listener.exitJoint_action_id(self)




    def joint_action_id(self):

        localctx = SlsgInputParser.Joint_action_idContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_joint_action_id)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 197
            self.match(SlsgInputParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





