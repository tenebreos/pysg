#!/usr/bin/python3

import unittest
import z3

from Agent import AgentConstraint, MultiAgentSystem
from Formula import Future, Forall, Exists, Atom, Quant, Bind, Always
from StvInterface import AddNodes, AddFormula, AddTransitions


class TestStvInterface(unittest.TestCase):
    def InitMas(self):
        AgentsConstraints = []
        AgentsConstraints.append(AgentConstraint(3, 2, 1))
        AgentsConstraints.append(AgentConstraint(2, 2, 1))
        Mas = MultiAgentSystem()
        for c in AgentsConstraints:
            Mas.AddAgent(c)
        Mas.SetInitialState(0, 0)
        Mas.ComputeModel()
        return Mas

    def InitModel(self, mas):
        model = z3.Model()
        for a in mas.agents:
            pb = [i for sl in a.pb for i in sl]
            vb = [i for sl in a.vb for i in sl]
            tb = [i for ssl in a.tb for sl in ssl for i in sl]
            for b in pb + tb + vb:
                model.update_value(b, False)

        m = model
        a1 = mas.agents[0]
        m.update_value(a1.pb[0][0], True)
        m.update_value(a1.pb[1][0], True)
        m.update_value(a1.pb[2][0], True)
        m.update_value(a1.pb[2][1], True)
        m.update_value(a1.tb[0][0][1], True)
        m.update_value(a1.tb[0][1][2], True)
        m.update_value(a1.tb[1][0][2], True)
        m.update_value(a1.tb[1][1][2], True)
        m.update_value(a1.tb[2][0][1], True)
        m.update_value(a1.tb[2][1][1], True)
        m.update_value(a1.tb[2][3][1], True)
        m.update_value(a1.tb[2][2][0], True)

        a2 = mas.agents[1]
        m.update_value(a2.pb[0][0], True)
        m.update_value(a2.pb[0][1], True)
        m.update_value(a2.pb[1][1], True)
        m.update_value(a2.tb[0][0][0], True)
        m.update_value(a2.tb[0][2][0], True)
        m.update_value(a2.tb[0][1][1], True)
        m.update_value(a2.tb[0][3][1], True)
        m.update_value(a2.tb[0][0][0], True)
        m.update_value(a2.tb[1][1][1], True)
        m.update_value(a2.tb[1][3][1], True)

        return model

    def FutureFormula(self):
        return Future(Quant(Forall('x'), Exists('y')),
                      Bind(('x', 0), ('y', 1)), Atom(0, 0))

    def GlobalFormula(self):
        return Always(Quant(Forall('x'), Exists('y')),
                      Bind(('x', 0), ('y', 1)), Atom(0, 0))

    def test_AddNodes_BothSets_ReturnsNodeDict(self):
        z1 = [0, 2, 3]
        z2 = [0, 1, 5]
        outDict = {}
        expectedDict = {
            "nodes": [
                {
                    "id": 0,
                    "label": "0",
                    "props": [0, 1]
                },
                {
                    "id": 1,
                    "label": "1",
                    "props": [1]
                },
                {
                    "id": 2,
                    "label": "2",
                    "props": [0]
                },
                {
                    "id": 3,
                    "label": "3",
                    "props": [0]
                },
                {
                    "id": 4,
                    "label": "4",
                    "props": []
                },
                {
                    "id": 5,
                    "label": "5",
                    "props": [1]
                }
            ]
        }
        mas = self.InitMas()
        AddNodes(z1, z2, mas, outDict)
        self.assertEqual(outDict, expectedDict)

    def test_AddFormula_Future_ReturnsValidDict(self):
        op = self.FutureFormula()
        mockDict = {}
        expectedDict = {
            "formula": {
                "quant_pref": [
                    ["Forall", "x"],
                    ["Exist", "y"]
                ],
                "bind_pref": [
                    ["x", 0],
                    ["y", 1]
                ],
                "form": {
                    "op": "F",
                    "operand1": {
                        "op": "p0"
                    }
                }
            }
        }
        AddFormula(op, mockDict)
        self.assertEqual(mockDict, expectedDict)

    def test_AddFormula_Global_ReturnsValidDict(self):
        op = self.GlobalFormula()
        mockDict = {}
        expectedDict = {
            "formula": {
                "quant_pref": [
                    ["Forall", "x"],
                    ["Exist", "y"]
                ],
                "bind_pref": [
                    ["x", 0],
                    ["y", 1]
                ],
                "form": {
                    "op": "G",
                    "operand1": {
                        "op": "p0"
                    }
                }
            }
        }
        AddFormula(op, mockDict)
        self.assertEqual(mockDict, expectedDict)

    def test_AddTransitions_SmallModel_ReturnsValidDict(self):
        mockDict = {}
        expectedDict = {
            'links': [
                {'label': '[0, 0]', 'source': 0, 'target': 2},
                {'label': '[0, 1]', 'source': 0, 'target': 5},
                {'label': '[0, 1]', 'source': 1, 'target': 5},
                {'label': '[0, 0]', 'source': 2, 'target': 4},
                {'label': '[0, 1]', 'source': 2, 'target': 5},
                {'label': '[0, 1]', 'source': 3, 'target': 5},
                {'label': '[0, 0]', 'source': 4, 'target': 2},
                {'label': '[0, 1]', 'source': 4, 'target': 3},
                {'label': '[1, 0]', 'source': 4, 'target': 0},
                {'label': '[1, 1]', 'source': 4, 'target': 3},
                {'label': '[0, 1]', 'source': 5, 'target': 3},
                {'label': '[1, 1]', 'source': 5, 'target': 3}
            ]
        }
        mas = self.InitMas()
        model = self.InitModel(mas)
        AddTransitions(mas, model, mockDict)
        self.assertEqual(mockDict, expectedDict)


if __name__ == "__main__":
    unittest.main()
