grammar SlsgInput;


/*
 * Syntax Root
 */
input_file : HEADER (line)+;

line 
    : agent_spec EOL
    | override EOL
    | formula EOL
    | EOL
    ;


/* 
 * Header 
 */
HEADER : 'p cnf 1 1' EOL '1 0' EOL;


/*
 * Agent spec
 */
agent_spec : 'kagent' agent_id local_states_no local_actions_no local_props_no;

NUMBER: [0-9]+;
agent_id : NUMBER;
local_states_no : NUMBER;
local_actions_no : NUMBER;
local_props_no : NUMBER;


/*
 * Formula
 */
formula : 'kslsg' slsg_formula;
slsg_formula 
    : quant_pref bind_pref 'F' slsg_formula  # FormulaFuture
    | quant_pref bind_pref 'G' slsg_formula  # FormulaGlobal
    | quant_pref bind_pref 'X' slsg_formula  # FormulaNext
    | quant_pref bind_pref '(' slsg_formula 'U' slsg_formula ')' # FormulaUntil
    | quant_pref bind_pref '(' slsg_formula 'R' slsg_formula ')' # FormulaRelease
    | '(' slsg_formula OP_AND slsg_formula ')'  # FormulaAnd
    | '(' slsg_formula OP_OR slsg_formula ')'   # FormulaOr
    | '(' slsg_formula OP_IMP slsg_formula ')'  # FormulaImpl
    | OP_NOT '(' slsg_formula ')'  # FormulaNot
    | prop  # FormulaProp
    | TRUE  # FormulaTrue
    | FALSE # FormulaFalse
    ;

OP_AND : 'AND' | '&' | '^';
OP_OR : 'OR' | '|' | 'v';
OP_NOT : 'NEG' | 'NOT' | '~';
OP_IMP : 'IMP' | '->';
TRUE : 'True' | 'TRUE' | 'true';
FALSE : 'False' | 'FALSE' | 'false';

prop : agent_id '.' prop_id;
prop_id : NUMBER;
quant_pref : (quant)+;
quant : '[' quantifier STRAT_VAR ']';
quantifier : 'E' | 'A';
STRAT_VAR : [a-z][a-z0-9_]*;
bind_pref : (bind)+;
bind : '<' STRAT_VAR ',' agent_id '>';


/*
 * Comment
 */
COMMENT : 'c' .*? EOL -> skip;


/*
 * Value Overrides
 */
override : val_override | prot_override | trans_override;
val_override : 'kval' VAL agent_id local_state_id prop_id;
prot_override : 'kprot' VAL agent_id local_state_id local_action_id;
trans_override : 'ktrans' VAL agent_id local_state_id joint_action_id local_state_id;
VAL : [+-];
local_state_id : NUMBER;
local_action_id : NUMBER;
joint_action_id : NUMBER;


/*
 * Whitespace
 */
WS : [ \t]+ -> skip;
EOL : '\r' ? '\n';
