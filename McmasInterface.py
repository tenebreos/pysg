#!/usr/bin/python3

# McmasInterface: an inteface to the MCMAS-1G model checker.

import re
import subprocess

# Procedures named '*Name' define naming conventions for ISPL variables.
# Procedures named 'Ispl*' generate parts of ISPL programs; the '*' part refers
# to a structure in ISPL syntax, eg. a section.


def indent(n):
    return '  ' * n


def CheckUsingMcmas(formula, mas, model):
    # 1. Create the ISPL file
    # 1.1. Iterate over agents
    # 1.2.1. Encode local states
    # 1.2.2. Encode actions
    # 1.2.3. Encode the protocol
    # 1.2.4. Encode the transition function
    # 2. Run MCMAS
    # 3. Return True iff model was found

    # Agent naming convention: Ai, where i - agent index.
    # State naming convention: lk, where k - local state index.
    # Action naming convention: at, where t - action index
    filename = GenerateIsplFile(formula, mas, model)
    return Check(filename)


def Check(filename):
    # TODO: this assumes that mcmas is installed globally on the system.
    cmd = ['mcmas64', filename]
    mcmas = subprocess.run(cmd, capture_output=True, text=True)
    regex_true = re.compile(r'Formula number 1: .*, is TRUE in the model')
    regex_false = re.compile(r'Formula number 1: .*, is FALSE in the model')
    formula_true = regex_true.search(mcmas.stdout) is not None
    formula_false = regex_false.search(mcmas.stdout) is not None
    if (not formula_true and not formula_false) or (formula_true and
                                                    formula_false):
        raise RuntimeError('Model checker output could not be parsed.')
    return formula_true


def GenerateIsplFile(formula, mas, model):
    filename = 'M.ispl'  # TODO: filename should be generated randomly.
    isplScript = open(filename, 'w')
    for a in mas.agents:
        isplScript.write(IsplAgent(model, a, mas))
        isplScript.write('\n')
    isplScript.write(IsplEvaluation(model, mas))
    isplScript.write('\n')
    isplScript.write(IsplInitialStates(mas))
    isplScript.write('\n')
    isplScript.write(IsplFormulae(formula))
    isplScript.close()
    return filename


def AgentName(agentNumber):
    return f'A{agentNumber}'


def ActionName(actionNumber):
    return f'a{actionNumber}'


def StateName(stateNumber):
    return f'l{stateNumber}'


def VariableName(agentNumber, variableNumber):
    return f'{AgentName(agentNumber)}pv{variableNumber}'


def IsplList(a):
    list = ', '.join(a)
    return '{ ' + list + ' }'


def IsplVars(agent):
    # Beware, Vars refer to the ISPL concept, not propositional variables of an
    # agent.
    stateNames = [StateName(k) for k in
                  range(agent.constraints.localStatesCount)]
    return (f"{indent(1)}Vars:\n"
            f"{indent(2)}state : {IsplList(stateNames)};\n"
            f"{indent(1)}end Vars")


def IsplActions(agent):
    actionNames = [ActionName(t) for t in
                   range(agent.constraints.actionsCount)]
    return f'{indent(1)}Actions = {IsplList(actionNames)};'


def IsplProtocol(model, agent):
    # 1. Iterate over states
    # 2. For every state interate over actions and associate permitted actions
    #    with that state.
    # 3. Map it to ISPL.
    stateActionPairs = []
    for k in range(agent.constraints.localStatesCount):
        allowedActions = []
        for t in range(agent.constraints.actionsCount):
            if (model.eval(agent.pb[k][t])):
                allowedActions.append(ActionName(t))
        stateActionPairs.append((k, allowedActions))
    section = f'{indent(1)}Protocol:\n'
    for k, acts in stateActionPairs:
        section += f'{indent(2)}state={StateName(k)} : {IsplList(acts)};\n'
    section += f'{indent(1)}end Protocol'
    return section


def IsplEvolution(model, agent, mas):
    # 1. Iterate over target states,
    # 2. For every target state, iterate over beginning states,
    # 3. For every target and begining state, iterate over joint actions,
    # 4. Write the target state, then for every begining state write it and the
    # joint actions which lead to it.

    struct = []  # (k', [(k, [every j which leads from k to k'])])
    for kp in range(agent.constraints.localStatesCount):
        struct2 = []
        for k in range(agent.constraints.localStatesCount):
            jointActions = []
            for j in range(len(mas.jointActions)):
                if (model.eval(agent.tb[k][j][kp])):
                    jointActions.append(mas.JointActionNumber2Tuple(j))
            struct2.append((k, jointActions))
        struct.append((kp, struct2))

    lf = '\n'

    section = f'{indent(1)}Evolution:\n'

    alpha = []
    for kp, kjs in struct:
        isKpReachable = False
        beta = []
        for k, js in kjs:
            if len(js) == 0:
                continue
            isKpReachable = True
            gamma = []
            for j in js:
                delta = []
                for i, a in enumerate(j):
                    delta.append(f'({AgentName(i)}.Action={ActionName(a)})')
                gamma.append(f'({" and ".join(delta)})')
            beta.append(f'(state={StateName(k)}) and (\n'
                        f'{indent(4)}{(lf+indent(5)+ "or" +lf+indent(4)).join(gamma)}\n'
                        f'{indent(3)})')
        if isKpReachable:
            alpha.append(f'{indent(2)}(state={StateName(kp)}) if\n'
                         f'{indent(3)}{(lf+indent(4)+ "or" +lf+indent(3)).join(beta)};\n')
    section += ''.join(alpha)
    section += f'{indent(1)}end Evolution'
    return section


def IsplInitialStates(mas):
    alpha = []
    for a, k in enumerate(mas.GlobalStateNumber2Tuple(mas.initialState)):
        alpha.append(f'({AgentName(a)}.state={StateName(k)})')
    section = 'InitStates\n'
    section += f'{indent(1)}' + f' and\n{indent(1)}'.join(alpha) + ';\n'
    section += 'end InitStates\n'
    return section


def IsplEvaluation(model, mas):
    alpha = []
    for a in mas.agents:
        for j in range(a.constraints.variablesCount):
            beta = []
            for k in range(a.constraints.localStatesCount):
                # TODO: model_completion here for testing
                if model.eval(a.vb[k][j], model_completion=True):
                    beta.append(f'({AgentName(a.index)}.state={StateName(k)})')
            if len(beta) != 0:
                alpha.append(f'{indent(1)}{VariableName(a.index, j)} if ' +
                             ' or '.join(beta) + ';')
    section = 'Evaluation\n'
    section += '\n'.join(alpha) + '\n'
    section += 'end Evaluation\n'
    return section


def IsplFormulae(formula):
    return (f'Formulae\n'
            f'  {formula};\n'
            f'end Formulae')


def IsplAgent(model, agent, mas):
    return (f"Agent {AgentName(agent.index)}\n"
            f"{IsplVars(agent)}\n"
            f"{IsplActions(agent)}\n"
            f"{IsplProtocol(model, agent)}\n"
            f"{IsplEvolution(model, agent, mas)}\n"
            f"end Agent\n")
