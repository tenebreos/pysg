#!/usr/bin/python3

# StvInterface: an interface to the STV model checker.

from Formula import UnaryTemporalOps, BinaryTemporalOps
from Agent import MultiAgentSystem
from Config import GetConfig
import os
import json
import subprocess

Next = UnaryTemporalOps[0]
Future = UnaryTemporalOps[1]
Always = UnaryTemporalOps[2]


def CheckUsingStvUnary(op, Z1, mas, model):
    # op: F, G, X
    if op.op == Next:
        raise ValueError("STV does not support the 'X' (Next) operator. " +
                         "Please, change your formula.")
    dictionary = {}
    AddNodes(Z1, [], mas, dictionary)
    AddTransitions(mas, model, dictionary)
    AddFormula(op, dictionary)
    jsonObject = json.dumps(dictionary)
    return CheckUsingStv(jsonObject)


def CheckUsingStvBinary(op, Z1, Z2, mas, model):
    raise ValueError("STV does not support the 'U' (Until) operator. " +
                     "Please, change your formula.")


def AddNodes(z1, z2, mas, dictionary):
    dictionary["nodes"] = []
    for s, _ in enumerate(mas.globalStates):
        state = {
            "id": s,
            "label": str(s),
            "props": []
        }
        if s in z1:
            state["props"].append(0)
        if s in z2:
            state["props"].append(1)

        dictionary["nodes"].append(state)


def AddTransitions(mas, model, dictionary):
    # Algoritm:
    # - Iterate over all global states
    # - For every state, iterate over all joint actions
    # - For every action, iterate over all global states
    # - For every state, iterate over agents and check if local parts match,
    # - Add the global states and the action as a transition iff local parts
    #   match.
    links = []
    for k, s1 in enumerate(mas.globalStates):
        for j, alpha in enumerate(mas.jointActions):
            for kp, s2 in enumerate(mas.globalStates):
                matches = True
                for i, ag in enumerate(mas.agents):
                    if not model.eval(ag.tb[s1[i]][j][s2[i]]):
                        matches = False
                if matches:
                    links.append({
                        "source": k,
                        "target": kp,
                        "label": str(list(alpha))
                    })
    dictionary['links'] = links


def AddFormula(op, dictionary):
    # op must be one of: F, G
    formula = {
        "quant_pref": [],
        "bind_pref": [],
    }
    for quantifier in op.quant:
        q = ("Exist" if quantifier.quantifier_type == "Exists" else
             "Forall")
        v = quantifier.var
        formula['quant_pref'].append([q, v])

    for binding in op.bind:
        s, a = binding
        formula['bind_pref'].append([s, a])

    stvOp = "F" if op.op == Future else "G"
    form = {
        "op": stvOp,
        "operand1": {
            "op": "p0"
        }
    }

    formula['form'] = form
    dictionary['formula'] = formula


def CheckUsingStv(jsonObject):
    # TODO: check if the environment call can be optimized
    config = GetConfig()
    stv_binary = config.ModelCheckerPath()
    filename = config.StvFilename()
    StvIn = open(filename, 'w')
    StvIn.write(jsonObject)
    StvIn.close()
    # TODO: make the following automatically find the name of a python binary
    cmd = ['python3', stv_binary, filename]
    subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    StvOut = open('out.txt', 'r')
    line = StvOut.readline()
    states = []
    for s in line.strip().split():
        states.append(int(s))
    return states


if __name__ == "__main__":
    from Formula import *
    from Agent import *
    from Phi import Phi
    import z3
    op = Future(Quant(Forall('x'), Exists('y')),
                Bind(('x', 0), ('y', 1)), Atom(0, 0))
    AgentsConstraints = []
    AgentsConstraints.append(AgentConstraint(3, 3, 1))
    AgentsConstraints.append(AgentConstraint(2, 2, 1))
    Mas = MultiAgentSystem()
    for c in AgentsConstraints:
        Mas.AddAgent(c)
    Mas.SetInitialState(0, 0)
    Mas.ComputeModel()
    solver = z3.Solver()
    solver.add(Phi(Mas))
    solver.check()
    model = solver.model()

    print(CheckUsingStvUnary(op, [0, 2, 3, 4], Mas, model))
