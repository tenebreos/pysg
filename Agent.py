#!/usr/bin/python3

# Agent: representations of an agent and a multi agent system.

from z3 import BoolSort, IntSort, Bool
from itertools import product

Z = IntSort()
B = BoolSort()


class AgentConstraint:
    def __init__(self, localStatesCount, actionsCount, variablesCount):
        self.localStatesCount = localStatesCount
        self.actionsCount = actionsCount
        self.variablesCount = variablesCount


class Agent:
    def __init__(self, index, agentConstraints):
        self.constraints = agentConstraints
        self.index = index
        # - PB_i: Z^2 -> B such that PB_i(k, t) = T iff action no. t is in the
        # protocol of state no. k of agent no. i.
        # - TB_i: Z^3 -> B such that TB_i(k, j, k') = T iff there exists a
        # transitions from state no. k to state no. k' of agent no. i under
        # global (aka. joint) action no. j.
        # - VB_i: Z^2 -> B such that VB_i(k, j) = T iff propositional variable
        # no. j of agent no. i is assigned to a state no. k of agent no. i.
        # - IB_i: Z -> B such that IB_i(k) = T iff state no. k is the initial
        # state of an agent.
        self.pb = [[Bool(f'pb{index}_{k}_{t}')
                    for t in range(agentConstraints.actionsCount)]
                   for k in range(agentConstraints.localStatesCount)]
        self.tb = []
        self.vb = [[Bool(f'vb{index}_{k}_{j}')
                    for j in range(agentConstraints.variablesCount)]
                   for k in range(agentConstraints.localStatesCount)]
        self.ib = [Bool(f'ib{index}_{k}')
                   for k in range(agentConstraints.localStatesCount)]


class MultiAgentSystem:
    def __init__(self):
        self.agents = []
        self.jointActions = []

    #  0-agent-num->4
    # (1, 2, 3, 4, 5)
    def __GenerateJointActions(self, agents):
        agentsActionCounts = map(lambda a: a.constraints.actionsCount, agents)
        tmp = tuple(map(lambda i: [k for k in range(i)], agentsActionCounts))
        jointActions = list(product(*tmp))
        return jointActions

    def __GenerateGlobalStates(self, agents):
        agentsStateCounts = map(lambda a: a.constraints.localStatesCount,
                                agents)
        tmp = tuple(map(lambda i: [k for k in range(i)], agentsStateCounts))
        globalStates = list(product(*tmp))
        return globalStates

    def __Tuple2Number(self, tupl, ms):
        # Let alpha = (t_1, t_2, ..., t_n), where n = len(agents).
        # Let m_i be the number of actions of agent i, and assume agents are
        # indexed starting on 1, i.e. 1 <= i <= n (they aren't in the program).
        # Then joint action number, j =
        #   t_n +
        #   t_{n-1} * m_n +
        #   t_{n-2} * m_{n-1} * m_n +
        #   ... +
        #   t_1 * m_2 * m_3 * ... * m_n
        # Same formula applies for conversion from a global state tuple to a
        # global state number.
        j = 0
        m = 1
        for i, t_i in list(enumerate(list(tupl)))[::-1]:
            j += t_i * m
            m *= ms[i]
        return j

    def JointActionTuple2Number(self, jointActionTuple):
        return self.__Tuple2Number(jointActionTuple,
                                   list(map(
                                       lambda i: i.constraints.actionsCount,
                                       self.agents)))

    def JointActionNumber2Tuple(self, jointActionNumber):
        return self.jointActions[jointActionNumber]

    def CorrespondingJointActions(self, localActionNumber, localActionIndex):
        '''
        Returns a list of numbers of joint actions containing local action t on
        index i.

        i: index of the local action in a joint action
        t: number of the local action
        mas: a MultiAgentSystem object
        '''
        max_i = len(self.agents)
        if localActionIndex >= max_i:
            raise ValueError(f'There are {max_i} agents. Agent '
                             f'{localActionIndex} does not exist since '
                             f'{localActionIndex} >= {max_i}.')
        max_t = self.agents[localActionIndex].constraints.actionsCount
        if localActionNumber >= max_t:
            raise ValueError(f'Agent {localActionIndex} has {max_t} actions. '
                             f'Action {localActionNumber} does not exist '
                             f'since {localActionNumber} >= {max_t}.')
        js = []
        for alpha in self.jointActions:
            if alpha[localActionIndex] == localActionNumber:
                js.append(self.JointActionTuple2Number(alpha))
        return js

    def GlobalStateTuple2Number(self, stateTuple):
        return self.__Tuple2Number(stateTuple,
                                   list(map(lambda i:
                                            i.constraints.localStatesCount,
                                            self.agents)))

    def GlobalStateNumber2Tuple(self, stateNumber):
        return self.globalStates[stateNumber]

    # TODO: This is the same as self.CorrespondingJointActions. Extract a
    # common part.
    def CorrespondingGlobalStates(self, localStateNumber, localStateIndex):
        '''
        Returns a list of numbers of global states containing local state l on
        index i.

        i: index of the local action in a joint action
        l: number of the local action
        mas: a MultiAgentSystem object
        '''
        max_i = len(self.agents)
        if localStateIndex >= max_i:
            raise ValueError(f'There are {max_i} agents. Agent '
                             f'{localStateIndex} does not exist since '
                             f'{localStateIndex} >= {max_i}.')
        max_t = self.agents[localStateIndex].constraints.localStatesCount
        if localStateNumber >= max_t:
            raise ValueError(f'Agent {localStateIndex} has {max_t} states. '
                             f'State {localStateNumber} does not exist '
                             f'since {localStateNumber} >= {max_t}.')
        js = []
        for alpha in self.globalStates:
            if alpha[localStateIndex] == localStateNumber:
                js.append(self.GlobalStateTuple2Number(alpha))
        return js

    def AddAgent(self, agentConstraint):
        self.agents.append(Agent(len(self.agents), agentConstraint))

    def SetInitialState(self, *initialGlobalState):
        for i, k in enumerate(initialGlobalState):
            max_state_num = self.agents[i].constraints.localStatesCount - 1
            if k > max_state_num:
                raise ValueError(f'Agent {i} has {max_state_num + 1} states.'
                                 f' State {k} does not exist for this agent.')
        if len(initialGlobalState) != len(self.agents):
            raise ValueError('The number of localStates in the initial global '
                             'state should be equal to the'
                             ' number of agents in the MultiAgentSystem.')
        self.initialState = self.GlobalStateTuple2Number(initialGlobalState)

    # TODO: write a test for ComputeModel.
    def ComputeModel(self):
        self.jointActions = self.__GenerateJointActions(self.agents)
        self.globalStates = self.__GenerateGlobalStates(self.agents)
        # TODO: ask: should the initial state always be set to 0?
        self.initialState = 0
        for a in self.agents:
            a.tb = [[[Bool(f'tb{a.index}_{k}_{j}_{kp}')
                      for kp in range(a.constraints.localStatesCount)]
                     for j in range(len(self.jointActions))]
                    for k in range(a.constraints.localStatesCount)]


# Some tests.
if __name__ == "__main__":
    from pprint import pprint
    AgentsConstraints = []
    AgentsConstraints.append(AgentConstraint(3, 2, 1))
    AgentsConstraints.append(AgentConstraint(2, 2, 1))

    Mas = MultiAgentSystem()
    for c in AgentsConstraints:
        Mas.AddAgent(c)
    for a in Mas.agents:
        print(f'Agent {a.index}:')
        print('\t- pb:')
        pprint(a.pb)
        print('\t- tb:')
        pprint(a.tb)
        print('\t- vb:')
        pprint(a.vb)
        print('\t- ib:')
        pprint(a.ib)
    Mas.ComputeModel()
    print(Mas.jointActions)
    for a in Mas.agents:
        print(f'Agent {a.index}:')
        print('\t- pb:')
        pprint(a.pb)
        print('\t- tb:')
        pprint(a.tb)
        print('\t- vb:')
        pprint(a.vb)
        print('\t- ib:')
        pprint(a.ib)
    print(Mas.agents[0].pb[1][0])
