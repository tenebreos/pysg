#!/usr/bin/python3

# InputParserIterface: provide an API for parsing Formula and
#  AgentConstraint objects from a stream.

from Config import GetConfig
from Benchmark import Benchmark
from AntlrInputParser import ParseWithAntlr


benchmark = None


def ParseInput(string):
    """
    Parameters:
    - stream - a stream which to parse.

    Returns:
    - tuple ([AgentConstraint], [PartialSpec], Formula)
    """
    global benchmark
    if benchmark:
        benchmark.Start()
    results =  ParseWithAntlr(string)
    if benchmark:
        benchmark.Stop()
        benchmark.Save('parse_input')
    return results


def EnableParserBenchmark():
    global benchmark
    benchmark = None if not GetConfig().Benchmark() else Benchmark()
