#!/usr/bin/python3

import unittest

from Formula import Bind, Quant, Quantifier, Forall, Exists, Next, Atom, And, Formula


class TestFormula(unittest.TestCase):
    def test_Forall_returnsAForallQuantifier(self):
        expected = Quantifier('Forall', 'x')
        real = Forall('x')
        self.assertEqual(expected, real)

    def test_Exists_returnsAnExistsQuantifier(self):
        expected = Quantifier('Exists', 'x')
        real = Exists('x')
        self.assertEqual(expected, real)

    def test_Bind_returnsAListOfBindings(self):
        expected = [('x', 0), ('y', 1)]
        real = Bind(('x', 0), ('y', 1))
        self.assertEqual(expected, real)

    def test_Bind_raisesValueErrorOnAnInvalidVar(self):
        with self.assertRaises(ValueError):
            Bind((True, 0))

    def test_Bind_raisesValueErrorOnAnInvalidAgent(self):
        with self.assertRaises(ValueError):
            Bind(('x', True))

    def test_Quant_returnsAListOfQuantifiers(self):
        expected = [Quantifier('Forall', 'x'), Quantifier('Exists', 'y')]
        real = Quant(Forall('x'), Exists('y'))
        self.assertEqual(expected, real)

    def test_Quant_raisesValueErrorOnAnInvalidQuantification(self):
        with self.assertRaises(ValueError):
            Quant(0)

    def test_Quantifier_errorsOnInvalidQuantifierType(self):
        with self.assertRaises(ValueError):
            Quantifier('a', 'x')

    def ExampleTemporalFormula(self):
        return Next(Quant(Forall('x'), Exists('y')), Bind(('x', 0), ('y', 1)),
                    Atom(0, 0))

    def ExampleBooleanFormula(self):
        return And(Atom(0, 0), Atom(1, 0))

    def test_Formula_AgentsUnderExistentialQuantificationReturnsProperAgents(self):
        phi = self.ExampleTemporalFormula()
        expected = [1]
        result = phi.AgentsUnderExistentialQuantification()
        self.assertEqual(expected, result)

    def test_Formula_AgentsUnderUniversalQuantificationReturnsProperAgents(self):
        phi = self.ExampleTemporalFormula()
        expected = [0]
        result = phi.AgentsUnderUniversalQuantification()
        self.assertEqual(expected, result)

    def test_Formula_AgentsUnderExistentialQuantificationErrorsOnANonTemporalFormula(self):
        phi = self.ExampleBooleanFormula()
        with self.assertRaises(ValueError):
            phi.AgentsUnderExistentialQuantification()

    def test_Formula_AgentsUnderUniversalQuantificationErrorsOnANonTemporalFormula(self):
        phi = self.ExampleBooleanFormula()
        with self.assertRaises(ValueError):
            phi.AgentsUnderUniversalQuantification()

    def ExpectedAndFormula(self):
        p = Atom(0, 0)
        f1 = Formula()
        f1.op = "And"

        f = Formula()
        f.op = "And"

        f2 = Formula()
        f2.op = "And"
        f2.arg1 = p
        f2.arg2 = p

        f1.arg1 = p
        f1.arg2 = f2

        f.arg1 = p
        f.arg2 = f1

        return f

    def test_And_returnsAProperTree(self):
        expected = self.ExpectedAndFormula()
        result = And(Atom(0, 0), Atom(0, 0), Atom(0, 0), Atom(0, 0))
        self.assertEqual(result, expected)


if __name__ == "__main__":
    unittest.main()
