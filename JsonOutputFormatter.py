#!/usr/bin/python3

# JsonOutputFormatter: format an induced model to JSON.

import json


def FormatModelInJson(mas, model):
    status = InitStatusDict()
    status['info']['status'] = 'SAT'

    for agent in mas.agents:
        element = InitElement(f"Agent {agent.index}")
        status['models'].append(element)
        AddAgentStates(agent, model, element['nodes'])
        AddAgentLinks(mas, model, agent, element['links'])

    element = InitElement("Global model")
    status['models'].append(element)
    AddModelStates(mas, model, element['nodes'])
    AddModelLinks(mas, model, element['links'])

    return json.dumps(status)


def AddAgentStates(agent, model, nodes):
    for state in range(agent.constraints.localStatesCount):
        props = []
        for atom_no, b in enumerate(agent.vb[state]):
            if model.eval(b):
                props.append(atom_no)
        nodes.append(Node(state, state, props))


def AddAgentLinks(mas, model, agent, links):
    link_id = 0
    for k in range(agent.constraints.localStatesCount):
        for kp in range(agent.constraints.localStatesCount):
            actions = []
            for j in range(len(mas.jointActions)):
                if model.eval(agent.tb[k][j][kp]):
                    actions.append(j)
            if len(actions) != 0:
                link = Link(link_id, k, kp, actions)
                links.append(link)
                link_id += 1


def AddModelStates(mas, model, nodes):
    for k, state in enumerate(mas.globalStates):
        state = list(state)
        props = []
        for agent_no, local_state_no in enumerate(state):
            for atom_no, b in enumerate(mas
                                        .agents[agent_no]
                                        .vb[local_state_no]):
                if model.eval(b):
                    props.append(f'{agent_no}.{atom_no}')
        nodes.append(Node(k, state, props))


def AddModelLinks(mas, model, links):
    link_id = 0
    for k, source in enumerate(mas.globalStates):
        for kp, target in enumerate(mas.globalStates):
            actions = []
            for j in range(len(mas.jointActions)):
                matches = True
                for i, agent in enumerate(mas.agents):
                    if not model.eval(agent.tb[source[i]][j][target[i]]):
                        matches = False
                if matches:
                    actions.append(j)
            if len(actions) != 0:
                link = Link(link_id, k, kp, actions)
                links.append(link)
                link_id += 1


def FormatUnsatMessage(mas):
    status = InitStatusDict()
    status['info']['status'] = 'UNSAT'
    return json.dumps(status)


def InitStatusDict():
    return {
        'info': {},
        'models': [],
    }


# Let 'element' (of MAS) be an agent or an induced model.
def InitElement(label):
    return {
        'label': label,
        'nodes': [],
        'links': []
    }


def Node(state_no, state_label, props):
    return {
        'id': state_no,
        'label': str(state_label),
        'props': props
    }


def Link(id, source_no, target_no, label):
    return {
        'id': id,
        'source': source_no,
        'target': target_no,
        'label': str(label)
    }
