#!/usr/bin/python3

# Config.py: module for holding configuration.

import os


class Config:
    def __init__(self):
        self.model_checker_path = None
        self.model_checker = None
        self.verbose = None
        self.benchmark = None
        self.filename = None
        self.stvFilename = 'StvIn.json'
        self.benchmarkFilename = 'benchmark.txt'

    def ParseArguments(self, parser):
        args = parser.parse_args()
        if args.model_checker_path:
            self.model_checker_path = args.model_checker_path
        else:
            self.model_checker_path = os.environ['PYSG_MODEL_CHECKER_PATH']
        self.model_checker = args.model_checker
        self.verbose = args.verbose
        self.benchmark = args.benchmark
        self.filename = args.filename

    def Filename(self):
        return self.filename

    def Benchmark(self):
        return self.benchmark

    def Verbose(self):
        return self.verbose

    def ModelChecker(self):
        return self.model_checker

    def ModelCheckerPath(self):
        return self.model_checker_path

    def StvFilename(self):
        return self.stvFilename

    def BenchmarkFilename(self):
        return self.benchmarkFilename


config = Config()


def GetConfig():
    return config
