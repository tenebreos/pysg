# About

Pysg is a prototype for an SL[SG] solver.

# Setup

Pysg depends on the following pip packages:

- `antlr4-python3-runtime`
- `z3-solver`

and the following programs:

- STV, available at: https://github.com/blackbat13/stv

Path to an `api_sl.py` file from STV must either be provided in the
`--model-checker-path` option or the `PYSG_MODEL_CHECKER_PATH` environment
variable. The option takes precedence over the environment variable.

# Usage

Pysg can be used in two ways:

```
$ cat input | python3 solver.py
```

or

```
$ python3 solver.py filename
```

If a filename is provided, pysg will read it's input from a specified file. If
filename is `-`, pysg will read STDIN. Pysg has two options:

- `--model-checker|-m` - defines which model checker to use, `stv` or `mcmas`.
    MCMAS is currently unsuported. Defaults to `stv`.
- `--model-checker-path|-p` - defines a path of a model checker executable.
