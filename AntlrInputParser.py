#!/usr/bin/python3

# AntlrInputParser: parse Formula and AgentConstraint objects from a stream
#  using Antlr.

from SlsgInputParser import SlsgInputParser
from SlsgInputLexer import SlsgInputLexer
from SlsgInputListener import SlsgInputListener
from antlr4 import (InputStream, CommonTokenStream, ParseTreeWalker)
from Formula import (Not, And, Or, Implies, Next, Until, Future, Always, Bind,
                     Quant, Forall, Exists, Atom)
from Agent import AgentConstraint


def ParseWithAntlr(string):
    input_stream = InputStream(string)
    lexer = SlsgInputLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = SlsgInputParser(token_stream)

    tree = parser.input_file()

    walker = ParseTreeWalker()
    listener = SatInputListener()
    walker.walk(listener, tree)

    constraints = listener.GetAgentConstraints()
    partial_specs = listener.GetPartialSpecs()
    formula = listener.GetFormula()

    if len(constraints) == 0:
        raise Exception("Input data must contain at least one agent " +
                        "constraint. Please, change your input.")

    return constraints, partial_specs, formula


class SatInputListener(SlsgInputListener):
    def __init__(self):
        self._constraints = {}
        self._partial_specs = []
        self._formula = None
        self._stack = []

        self._orderExceptionMessage = ("Order of syntactic constructs in the " +
                                       "input is as follows: agent " +
                                       "constraints, partial specs, " +
                                       "formula. Your input doesn't match " +
                                       "this order. Please, change your " +
                                       "input.")
        # TODO: rename 'agentConstraints' to make nomenclature more consistent.
        self._agentConstraintsFound = False
        self._partialSpecsFound = False
        self._formulaFound = False

    def GetAgentConstraints(self):
        agent_constraints = []
        for i in range(len(self._constraints)):
            agent_constraints.append(self._constraints[i])
        return agent_constraints

    def GetPartialSpecs(self):
        return []

    def GetFormula(self):
        if len(self._stack) > 1:
            raise Exception("The stack should contain a single item. " +
                            "Something went wrong.")
        return self._stack.pop()

    def enterAgent_spec(self, ctx):
        agent_id = int(ctx.agent_id().NUMBER().getText())
        states_no = int(ctx.local_states_no().NUMBER().getText())
        actions_no = int(ctx.local_actions_no().NUMBER().getText())
        props_no = int(ctx.local_props_no().NUMBER().getText())
        c = AgentConstraint(states_no, actions_no, props_no)

        self._agentConstraintsFound = True

        if self._partialSpecsFound or self._formulaFound:
            raise Exception(self._orderExceptionMessage)

        if agent_id in self._constraints.keys():
            raise Exception("Agent constraint must be defined only once per " +
                            "each agent. Please, change your input data")

        self._constraints[agent_id] = c

    def enterFormula(self, ctx):
        if self._formulaFound:
            raise Exception("Input data must contain a single formula " +
                            "specification. Please, change your input.")
        elif not self._agentConstraintsFound:
            raise Exception(self._orderExceptionMessage)
        else:
            self._formulaFound = True

    def _processQuantPref(self, quant_pref):
        out = []
        for q in quant_pref.getChildren():
            quantifier = q.quantifier().getText()
            strat_var = q.STRAT_VAR().getText()
            if quantifier == 'A':
                out.append(Forall(strat_var))
            elif quantifier == 'E':
                out.append(Exists(strat_var))
            else:
                raise ValueError(f"Unknown quantfier: '{quantifier}'.")
        return out

    def _processBindPref(self, bind_pref):
        out = []
        for b in bind_pref.getChildren():
            strat_var = b.STRAT_VAR().getText()
            agent_id = int(b.agent_id().NUMBER().getText())
            out.append((strat_var, agent_id))
        return out

    def exitFormulaFuture(self, ctx):
        quant_pref = self._processQuantPref(ctx.quant_pref())
        bind_pref = self._processBindPref(ctx.bind_pref())
        arg = self._stack.pop()
        f = Future(quant_pref, bind_pref, arg)
        self._stack.append(f)

    def exitFormulaGlobal(self, ctx):
        quant_pref = self._processQuantPref(ctx.quant_pref())
        bind_pref = self._processBindPref(ctx.bind_pref())
        arg = self._stack.pop()
        f = Always(quant_pref, bind_pref, arg)
        self._stack.append(f)

    def exitFormulaNext(self, ctx):
        quant_pref = self._processQuantPref(ctx.quant_pref())
        bind_pref = self._processBindPref(ctx.bind_pref())
        arg = self._stack.pop()
        f = Next(quant_pref, bind_pref, arg)
        self._stack.append(f)

    def exitFormulaUntil(self, ctx):
        quant_pref = self._processQuantPref(ctx.quant_pref())
        bind_pref = self._processBindPref(ctx.bind_pref())
        arg2 = self._stack.pop()
        arg1 = self._stack.pop()
        f = Until(quant_pref, bind_pref, arg1, arg2)
        self._stack.append(f)

    def exitFormulaRelease(self, ctx):
        raise ValueError("Error: Operator 'Release' is not supported. " +
                         "Please, change your formula.")

    def exitFormulaAnd(self, ctx):
        arg2 = self._stack.pop()
        arg1 = self._stack.pop()
        f = And(arg1, arg2)
        self._stack.append(f)

    def exitFormulaOr(self, ctx):
        arg2 = self._stack.pop()
        arg1 = self._stack.pop()
        f = Or(arg1, arg2)
        self._stack.append(f)

    def exitFormulaImpl(self, ctx):
        arg2 = self._stack.pop()
        arg1 = self._stack.pop()
        f = Implies(arg1, arg2)
        self._stack.append(f)

    def exitFormulaNot(self, ctx):
        arg = self._stack.pop()
        f = Not(arg)
        self._stack.append(f)

    def exitFormulaProp(self, ctx):
        prop = ctx.prop()
        agent_id = int(prop.agent_id().NUMBER().getText())
        prop_id = int(prop.prop_id().NUMBER().getText())
        self._stack.append(Atom(agent_id, prop_id))

    def exitFormulaTrue(self, ctx):
        raise NotImplementedError("Error: Constant 'True' is not supported. " +
                                  "Please, change your formula.")

    def exitFormulaFalse(self, ctx):
        raise NotImplementedError("Error: Constant 'False' is not " +
                                  "supported. Please, change your formula.")

    def enterVal_override(self, ctx):
        raise NotImplementedError("Error: Partial specs are not supported. " +
                                  "Please, remove them from provided input " +
                                  "file.")

    def enterProt_override(self, ctx):
        raise NotImplementedError("Error: Partial specs are not supported. " +
                                  "Please, remove them from provided input " +
                                  "file.")

    def enterTrans_override(self, ctx):
        raise NotImplementedError("Error: Partial specs are not supported. " +
                                  "Please, remove them from provided input " +
                                  "file.")
