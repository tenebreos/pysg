#!/usr/bin/python3

# solver.py: a model synthesis tool (aka. solver) for SL[SG].

from Agent import AgentConstraint, MultiAgentSystem
from Formula import (Forall, Exists, Quant, Bind, Until, Not, And, Next,
                     Always, Atom, Future, Or)
from ModelGenerator import Solve, GetModel
from InputParserInterface import ParseInput, EnableParserBenchmark
from OutputFormatterInterface import (PrintSat, PrintUnsat,
                                      EnableOutpuFormmaterBenchmark)
from ModelCheckerInterface import (RunModelCheckerBenchmark,
                                   EndModelCheckerBenchmark)
from Config import GetConfig

import os
import sys
import argparse


if __name__ == "__main__":
    # Parse arguments:
    argparser = argparse.ArgumentParser()
    argparser.add_argument('filename', nargs='?', default='-',
                           help="Name of the input file; '-' for STDIN.")
    argparser.add_argument('-m', '--model-checker', default='stv',
                           choices=['stv', 'mcmas'],
                           help='Model checker to be used.')
    argparser.add_argument('-p', '--model-checker-path',
                           help='Set path of a model checker.')
    argparser.add_argument('-v', '--verbose', action='store_true',
                           help='Enable verbose output.')
    argparser.add_argument('-b', '--benchmark', action='store_true',
                           help="Enable benchmarking mode. Results are " +
                           "written to 'benchmark.txt'")
    config = GetConfig()
    config.ParseArguments(argparser)

    if config.ModelChecker() == 'mcmas':
        raise ValueError('MCMAS-SG[1G] is not currently supported.')

    input_string = None
    filename = config.Filename()
    if filename == '-':
        input_string = sys.stdin.read()
    else:
        input_file = open(filename, 'r')
        input_string = input_file.read()

    # Benchmark
    doBenchmarking = config.Benchmark()
    if doBenchmarking:
        EnableOutpuFormmaterBenchmark()
        EnableParserBenchmark()
        RunModelCheckerBenchmark()

    # Parse input:
    constraints, partial_specs, formula = ParseInput(input_string)

    # Create MAS:
    mas = MultiAgentSystem()
    for c in constraints:
        mas.AddAgent(c)
    mas.ComputeModel()

    if config.Verbose():
        print(f"Searching for a model for a formula {formula}.")

    # Search for a model:
    if Solve(mas, formula):
        PrintSat(mas, GetModel())
    else:
        PrintUnsat(mas)

    if doBenchmarking:
        EndModelCheckerBenchmark()
