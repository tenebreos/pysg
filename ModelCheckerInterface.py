#!/usr/bin/python3

# ModelCheckerInterface: provide a generic interface to a model checker.

from Benchmark import Benchmark
from McmasInterface import CheckUsingMcmas
from StvInterface import CheckUsingStvUnary, CheckUsingStvBinary


benchmark = None


def RunModelCheckerBenchmark():
    global benchmark
    benchmark = Benchmark()


def EndModelCheckerBenchmark():
    benchmark.Save('model_check')


# ModelCheckUnary and ModelCheckBinary are the same function conceptually.
# ModelCheckUnary is for processing unary operators, and ModelCheckBinary for
# processing binary ones. Z1 and Z2 are sets of global states in which
# arguments of the operators are fulfilled, eg.: for formula 'p and q', Z1 is
# the set of states in which p holds, and Z2 is the set of states in which q
# holds.
def ModelCheckUnary(op, Z1, mas, model):
    if benchmark:
        benchmark.Start()
    results =  CheckUsingStvUnary(op, Z1, mas, model)
    if benchmark:
        benchmark.Stop()
    return results


def ModelCheckBinary(op, Z1, Z2, mas, model):
    if benchmark:
        benchmark.Start()
    results = CheckUsingStvBinary(op, Z1, Z2, mas, model)
    if benchmark:
        benchmark.Stop()
    return results
