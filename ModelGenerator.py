#!/usr/bin/python3

# ModelGenerator: generate and test induced models to find one satisfing a
# provided formula.

from Benchmark import Benchmark
from Config import GetConfig
from ModelCheckerInterface import (RunModelCheckerBenchmark,
                                   EndModelCheckerBenchmark)
from Phi import Phi
from Apx import Apx

from copy import copy
import z3

Solution = None


def FindModel(mas, model, psi, vb, j):
    new_model = copy(model)

    if j == len(vb):
        global Solution
        Solution = new_model
        return True

    for v in [True, False]:
        new_model.update_value(vb[j], v)
        apx = Apx(psi, mas, new_model, 'over')
        i = mas.initialState
        if i in apx and FindModel(mas, new_model, psi, vb, j + 1):
            return True
    return False


def _FindModel(mas, model, psi, vb, j, benchmark):
    if benchmark:
        benchmark.Start()
    result = FindModel(mas, model, psi, vb, j)
    if benchmark:
        benchmark.Stop()
    return result


def Solve(mas, psi):
    benchmarkingEnabled = GetConfig().Benchmark()

    benchmark = None if not benchmarkingEnabled else Benchmark()
    z3benchmark = None if not benchmarkingEnabled else Benchmark()
    findModelBenchmark = None if not benchmarkingEnabled else Benchmark()

    if benchmark:
        benchmark.Start()

    solver = z3.Solver()
    solver.add(Phi(mas))
    vb = []
    for a in mas.agents:
        vb += [b for l1 in a.vb for b in l1]

    while CallZ3(solver, z3benchmark) == z3.sat:
        model = solver.model()
        if _FindModel(mas, model, psi, vb, 0, findModelBenchmark):
            break
        AddNegation(solver, model)

    if benchmark:
        benchmark.Stop()
        benchmark.Save('solve')
        z3benchmark.Save('sat')
        findModelBenchmark.Save('backtracking')

    global Solution
    if Solution is not None:
        return True
    else:
        return False


def CallZ3(solver, benchmark):
    if benchmark:
        benchmark.Start()
    result = solver.check()
    if benchmark:
        benchmark.Stop()
    return result


def GetModel():
    """
    Returns a model for a SL[SG] formula. Call is valid only when Solve was
    called previously and returned True.

    Returns: ModelRef
    """
    global Solution
    if Solution is None:
        raise ValueError("GetModel can only be called after a call to Solve, "
                         "which returned True.")
    return Solution


def AddNegation(solver, model):
    clauses = []
    for d in model.decls():
        # d(): FuncDeclRef -> BoolRef, odd and unintuitive.
        if model[d]:
            clauses.append(d())
        else:
            clauses.append(z3.Not(d()))
    f = z3.Not(z3.And(clauses))
    solver.add(f)
