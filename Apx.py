#!/usr/bin/python3

# TODO: change the description to something more fitting.
# Apx: implementations of Apx an related algorithms.

import copy
from z3 import is_true, is_false

from ModelCheckerInterface import ModelCheckUnary, ModelCheckBinary
from Formula import Atom, UnaryTemporalOps


def Not(lam):
    return 'over' if lam == 'under' else 'under'


# TODO: extend to support F and G.
def Apx(psi, mas, model, lam):
    """
    Calculate a lambda-approximation of ||phi||_M.

    Lambda is in {upper, lower}, M is a model of a MAS and ||phi||_M is the
    set of all states of M in which formula phi is satisfied.

    psi -- formula to check, a Formula object
    model -- a MultiAgentSystem object
    lam -- a string, either "over" or "under"
    """
    if isinstance(psi, Atom):
        allAgents = list(range(len(mas.agents)))
        return SatisfingStates(psi, mas, ModelApproximation(mas, model, lam,
                                                            allAgents))
    elif psi.op == 'Not':
        z = Apx(psi.arg1, mas, model, Not(lam))
        return list(set(range(len(mas.globalStates))).difference(set(z)))

    elif psi.op in UnaryTemporalOps:
        z = Apx(psi.arg1, mas, model, lam)
        E = psi.AgentsUnderExistentialQuantification()
        return ModelCheckUnary(psi, z, mas,
                               ModelApproximation(mas, model, lam, E))
    elif psi.op == 'Until':
        z1 = Apx(psi.arg1, mas, model, lam)
        z2 = Apx(psi.arg2, mas, model, lam)
        E = psi.AgentsUnderExistentialQuantification()
        return ModelCheckBinary('Until', z1, z2, mas,
                                ModelApproximation(mas, model, lam, E))
    elif psi.op == 'And':
        z1 = Apx(psi.arg1, mas, model, lam)
        z2 = Apx(psi.arg2, mas, model, lam)
        return list(set(z1).intersection(set(z2)))
    else:
        raise ValueError(f"Operator '{psi.op}' is not supported. Please, " +
                          "modify your formula.")


def ModelApproximation(mas, model, lam, gamma):
    if lam not in ['over', 'under']:
        raise ValueError("Argument 'lam' must be one of: 'over', 'under'.")
    default = lam == 'over'
    aprox = copy.copy(model)
    for a in mas.agents:
        for vb in [p for l1 in a.vb for p in l1]:
            UpdateModel(vb, aprox, default)
        pb_and_tb = ([p for l1 in a.pb for p in l1] +
                     [p for l1 in a.tb for l2 in l1 for p in l2])
        for b in pb_and_tb:
            if a.index in gamma:
                UpdateModel(b, aprox, default)
            else:
                UpdateModel(b, aprox, not default)
    return aprox


def UpdateModel(b, model, default):
    val = model.eval(b)
    if not (is_true(val) or is_false(val)):
        val = default
    model.update_value(b, val)


def SatisfingStates(atom, mas, model):
    '''
    Returns a list of numers of global states of `mas` which satisfy `atom`.

    atom: a Formula.Atom object
    mas: a MultiAgentSystem object
    '''
    max_agent_num = len(mas.agents)
    if atom.agent_id >= max_agent_num:
        raise ValueError(f'There are {max_agent_num} agents. Agent '
                         f'{atom.agent_id} does not exist since '
                         f'{atom.agent_id} >= {max_agent_num}.')
    max_atom_num = mas.agents[atom.agent_id].constraints.variablesCount
    if atom.prop_id >= max_atom_num:
        raise ValueError(f'Agent {atom.agent_id} has {max_atom_num} states. '
                         f'State {atom.agent_id} does not exist '
                         f'since {atom.agent_id} >= {max_atom_num}.')

    vb = mas.agents[atom.agent_id].vb
    satisfingStates = []
    for state_num, vars_in_state in enumerate(vb):
        if model.eval(vars_in_state[atom.prop_id]):
            satisfingStates += (mas.CorrespondingGlobalStates(state_num,
                                                              atom.agent_id))
    return satisfingStates
