#!/usr/bin/python3

# benchmark: benchmark pysg.

import argparse
import subprocess
import time
import datetime
import os

benchmark_file = 'benchmark.txt'


def TestPysg(tt, out, formula):
    out_file = open(out, 'w')
    out_file.write('agents,states,actions,props,total,parse,' +
                   'format,solve,check,sat,backtrack\n')

    n = 1
    s, a, p = DefaultComponents()
    t1 = RunPysg(n, s, a, p, formula, tt)
    while t1 != -1:
        SaveResult(out_file, t1, n, s, a, p)
        s, a, p = UpdateComponents(s, a, p)
        t2 = RunPysg(n, s, a, p, formula, tt)
        while t2 != -1:
            SaveResult(out_file, t2, n, s, a, p)
            s, a, p = UpdateComponents(s, a, p)
            t2 = RunPysg(n, s, a, p, formula, tt)

        s, a, p = DefaultComponents()
        n += 1
        t1 = RunPysg(n, s, a, p, formula, tt)

    out_file.close()


def UpdateComponents(s, a, p):
    if s == a == p + 1:
        return s, a, p + 1
    elif s == a == p:
        return s, a + 1, p
    elif s == a - 1 == p:
        return s + 1, a, p
    else:
        raise ValueError()


def DefaultComponents():
    return 2, 2, 1


def DeleteBenchmarkOutput():
    if os.path.exists(benchmark_file):
        os.remove(benchmark_file)


def ParseBenchmarkOutput():
    results = open(benchmark_file, 'r')

    parse_time = None
    format_time = None
    solve_time = None
    model_check_time = None
    sat_time = None
    backtracking_time = None

    for line in results:
        fields = line.split()
        if len(fields) != 3 and fields[1] != '=':
            raise ValueError(f"'{benchmark_file}' has an invalid format. Please " +
                             "check it for correctness.")
        key = fields[0]
        value = int(fields[2])
        if key == 'parse_input':
            parse_time = value
        elif key == 'print_output':
            format_time = value
        elif key == 'solve':
            solve_time = value
        elif key == 'model_check':
            model_check_time = value
        elif key == 'sat':
            sat_time = value
        elif key == 'backtracking':
            backtracking_time = value
        else:
            raise ValueError(f"Invalid field: {fields[0]}")
    results.close()
    return (parse_time, format_time, solve_time, model_check_time, sat_time,
            backtracking_time)


def SaveResult(out_file, t, n, s, a, p):
    tp, tf, ts, tm, tsat, tb = ParseBenchmarkOutput()
    out_file.write(f'{n},{s},{a},{p},{t},{tp},{tf},{ts},{tm},{tsat},{tb}\n')
    DeleteBenchmarkOutput()


def Log(message):
    now = datetime.datetime.now().strftime('%H:%M:%S.%f')
    print(f'[{now}] {message}')


def RunPysg(n, s, a, p, formula, tt):
    '''
    Runs pysg. Let t - run time. Returns t iff t <= tt and -1 otherwise.
    '''
    Log(f'Testing for {n} agents, ({s}, {a}, {p}) components.')
    filename = 'pysg.txt'
    GeneratePysgInput(filename, n, s, a, p, formula)
    # NOTE: Let b - this script, s - solver.py. The following line assumes a
    # fixed relationship between locations of b and s. As such, b must be run
    # from the directory it is in, or it won't work.
    # TODO: make solver.py call more robust
    argv = ['python3', '../solver.py', '--benchmark', filename]
    try:
        run_time = time.time_ns()
        subprocess.run(argv, timeout=tt)#,
                       # stdout=subprocess.DEVNULL,
                       # stderr=subprocess.DEVNULL)
        run_time = time.time_ns() - run_time
        return run_time
    except subprocess.TimeoutExpired:
        Log('Timeout reached.')
        return -1


def GeneratePysgInput(filename, n, s, a, p, formula):
    file = open(filename, 'w')
    file.write('p cnf 1 1\n1 0\n\n')
    for i in range(n):
        file.write(f'kagent {i} {s} {a} {p}\n')
    file.write(f'\nkslsg {formula(n)}\n')


def Psi1(n):
    bind_pref = []
    quant_pref = []
    for i in range(n):
        svar = Svar(i)
        quant_pref.append(Quant('E', svar))
        bind_pref.append(Bind(svar, i))
    formula = (' '.join(quant_pref) + ' ' + ' '.join(bind_pref) +
               ' F ' + Ands(n))
    return formula


def Psi2(n):
    quant_pref = []
    bind_pref = []
    svar = Svar(0)
    quant_pref.append(Quant('A', svar))
    bind_pref.append(Bind(svar, 0))
    for i in range(1, n):
        svar = Svar(i)
        quant_pref.append(Quant('E', svar))
        bind_pref.append(Bind(svar, i))
    formula = (' '.join(quant_pref) + ' ' + ' '.join(bind_pref) +
               ' F ' + Ands(n))
    return formula


def Ands(n):
    ands = []
    end_braces = []
    for i in range(n - 1):
        ands.append(f'({i}.0 AND')
        end_braces.append(')')
    ands.append(f'{n - 1}.0')
    formula = ' '.join(ands) + ''.join(end_braces)
    return formula


def Quant(q, s):
    return f'[{q} {s}]'


def Bind(s, i):
    return f'<{s}, {i}>'


def Svar(i):
    return f's{i}'


def CleanupFiles():
    unneeded_files = ['out.txt', 'pysg.txt', 'StvIn.json', benchmark_file]
    Log('Cleaning up temporary files:')
    for file in unneeded_files:
        if os.path.exists(file):
            os.remove(file)
            Log(f'Removing {file}.')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('max_runtime', type=int,
                        help="Maximum allowed runtime of pysg, in seconds.")
    args = parser.parse_args()
    tt = args.max_runtime

    if 'PYSG_MODEL_CHECKER_PATH' not in os.environ:
        Log('Error! PYSG_MODEL_CHECKER_PATH environment variable must be ' +
            'defined in order to run this script. For meaning of the ' +
            'variable, please refer to README.md.')
        exit(-1)

    d = os.path.dirname(os.path.realpath(__file__))
    os.chdir(d)

    today = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M')
    ext = '.csv'
    TestPysg(tt, f'Psi1_{today}{ext}', Psi1)
    TestPysg(tt, f'Psi2_{today}{ext}', Psi2)
    CleanupFiles()
